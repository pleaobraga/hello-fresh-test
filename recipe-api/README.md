# Recipe API Server

This Api was created to provide some recipes.

## Start Application

To get started Application right away:

* Install and start the API server
    - `npm install`
    - `npm start`

The Api uses port the 3333 as default

### API Endpoint

The following endpoints are available:

| Endpoints       | Usage          | Params         |
|-----------------|----------------|----------------|
| `GET /` | Get all reciepes. Useful for the recipes page. |  |


### Explanations

This is a simple Api to provide some recipes,
it uses the json file provided by Hello Fresh

I builded it simple because my main goal was build the
front end application.

### Future Improvements

To improve this api I could use SuperAgent
and request data direct from hello fresh api. 

