/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("react-jss");

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.pluralWord = exports.renderNumber = exports.formatWeek = exports.addPropertyToArray = exports.renderIfExist = exports.isEmpty = exports.formatDuration = exports.getStringNumbers = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * return numbers inside the string
 * 
 * @param {string} string 
 * @returns {int}
 */
var getStringNumbers = exports.getStringNumbers = function getStringNumbers(string) {
    return isEmpty(string.match(/\d+/)) ? null : string.match(/\d+/g).join('');
};

/**
 *  return recipe Duration formated 
 * 
 * @param {any} stringDuration 
 * @returns {string}
 */
var formatDuration = exports.formatDuration = function formatDuration() {
    var stringDuration = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "s";

    var duration = getStringNumbers(stringDuration),
        unity = stringDuration[stringDuration.length - 1],
        fullUnity = void 0;

    switch (unity) {

        case "M":
            fullUnity = "minute";
            break;

        default:
            fullUnity = "";
            break;
    }

    return duration + ' ' + (duration != 1 ? fullUnity + 's' : fullUnity);
};

/**
 * verify if this element
 * is empty
 * 
 * @param {any} element 
 * @returns 
 */
var isEmpty = exports.isEmpty = function isEmpty(element) {
    return element === null || element === undefined || element === "";
};

/**
 * return a custom html element
 * with its property if 
 * is not null or undefined
 * 
 * @param {any} property 
 * @param {string} htmlTag 
 * @returns 
 */
var renderIfExist = exports.renderIfExist = function renderIfExist(property, htmlTag) {
    var classAtribute = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';


    var CustomTag = '' + htmlTag;

    return isEmpty(property) ? null : _react2.default.createElement(
        CustomTag,
        { className: classAtribute },
        property
    );
};

/**
 * create a personalized object
 * and add it to array
 * 
 * @param {any} property 
 * @param {string} name 
 * @param {array} array 
 */
var addPropertyToArray = exports.addPropertyToArray = function addPropertyToArray(property, name, array) {
    if (!isEmpty(property)) {
        var obj = {};

        obj[name] = property;
        array.push(obj);
    }
};

/**
 * format string week
 * 
 * @param {string} weeks 
 * @returns {string}
 */
var formatWeek = exports.formatWeek = function formatWeek(weeks) {
    if (!_lodash2.default.isEmpty(weeks)) {
        return weeks.map(function (week) {

            week = week.split("-W");

            return week[1] + ' week in ' + week[0];
        });
    }
};

/**
 * return number even 
 * variable is empty
 * 
 * @param {int} number 
 * @returns {int}
 */
var renderNumber = exports.renderNumber = function renderNumber(number) {
    return isEmpty(number) ? 0 : number;
};

/**
 * render correctly plural word
 * in context
 * add plural if necessary 
 * 
 * @param {number} number 
 * @param {string} word 
 * @param {string} pluralTermination 
 * @returns {string}
 */
var pluralWord = exports.pluralWord = function pluralWord(number, word) {
    var pluralTermination = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 's';

    return '' + word + (renderNumber(number) === 1 ? '' : pluralTermination);
};

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.List = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _utilFunctions = __webpack_require__(3);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _ListStyles = __webpack_require__(44);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless Component
 * return List base
 * 
 * @param {object} props 
 * @returns {jsx}
 */
var List = exports.List = function List(props) {

    /**
     * render lists  
     * 
     * @param {object} objectList 
     * @returns {jsx}
     */
    var renderLists = function renderLists(objectList) {
        return _lodash2.default.map(props.objectList, function (list, index) {
            return _react2.default.createElement(
                'div',
                { key: index, className: ' list-item ' + (index > 0 ? 'margin-item' : '') },
                (0, _utilFunctions.renderIfExist)(list.title, "h3"),
                _react2.default.createElement(
                    'ul',
                    null,
                    renderItensList(list, index)
                )
            );
        });
    };

    /**
     * render itens list
     * 
     * @param {object} list 
     * @param {integer} index 
     * @returns 
     */
    var renderItensList = function renderItensList(list, index) {
        return list.itens.map(function (item, index) {
            return _react2.default.createElement(
                'li',
                { key: index },
                item
            );
        });
    };

    return _react2.default.createElement(
        'div',
        { className: props.classes.list + '  ' + props.anotherClassName },
        renderLists(props.objectList)
    );
};

List.propTypes = {
    objectList: _propTypes2.default.object.isRequired,
    anotherClassName: _propTypes2.default.string,
    classes: _propTypes2.default.object
};

exports.default = (0, _reactJss2.default)(_ListStyles.styles)(List);

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.voteRecipe = exports.highlight = exports.fetchRecipes = undefined;

var _constants = __webpack_require__(8);

var _recipes = __webpack_require__(30);

var _recipes2 = _interopRequireDefault(_recipes);

var _RecipesApi = __webpack_require__(31);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

/**
 * dispatch recipes data 
 * 
 */
/*export const fetchRecipes = () => (dispatch, getState)  => {
    const res = {
        data: recipes
    }

    dispatch({
        type: FETCH_RECIPES,
        payload: res
    })
}*/

/**
 * dispatch recipes data 
 * 
 */
var fetchRecipes = exports.fetchRecipes = function fetchRecipes() {
    return function () {
        var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(dispatch, getState) {
            var res;
            return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            _context.next = 2;
                            return (0, _RecipesApi.getRecipeData)();

                        case 2:
                            res = _context.sent;


                            dispatch({
                                type: _constants.FETCH_RECIPES,
                                payload: res
                            });

                        case 4:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, undefined);
        }));

        return function (_x, _x2) {
            return _ref.apply(this, arguments);
        };
    }();
};

/**
 * Dispatch the highlight value
 * 
 * @param {string} id 
 * @param {bool} value 
 */
var highlight = exports.highlight = function highlight(id, value) {
    return function (dispatch, getState) {
        dispatch({
            type: _constants.TOGGLE_FAVORITE,
            id: id,
            value: value
        });
    };
};

/**
 * dispatch the vote 
 * 
 * @param {string} id 
 * @param {integer} vote 
 */
var voteRecipe = exports.voteRecipe = function voteRecipe(id, vote) {
    return function (dispatch, getState) {
        dispatch({
            type: _constants.VOTE_RECIPE,
            id: id,
            vote: vote
        });
    };
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var FETCH_RECIPES = exports.FETCH_RECIPES = "FETCH_RECIPES";
var VOTE_RECIPE = exports.VOTE_RECIPE = "VOTE_RECIPE";
var TOGGLE_FAVORITE = exports.TOGGLE_FAVORITE = "TOGGLE_FAVORITE";
var URL_API = exports.URL_API = "http://localhost:3333";

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Rating = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRedux = __webpack_require__(5);

var _index = __webpack_require__(7);

var _star = __webpack_require__(34);

var _star2 = _interopRequireDefault(_star);

var _starHalfEmpty = __webpack_require__(35);

var _starHalfEmpty2 = _interopRequireDefault(_starHalfEmpty);

var _starO = __webpack_require__(36);

var _starO2 = _interopRequireDefault(_starO);

var _utilFunctions = __webpack_require__(3);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _RatingStyles = __webpack_require__(37);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * React Class Component
 * deals with rating
 * render a icon for it 
 * and handle with its states
 * 
 * @class Rating
 * @extends {Component}
 */
var Rating = exports.Rating = function (_Component) {
    _inherits(Rating, _Component);

    function Rating() {
        _classCallCheck(this, Rating);

        var _this = _possibleConstructorReturn(this, (Rating.__proto__ || Object.getPrototypeOf(Rating)).call(this));

        _this.state = {
            score: 0
        };
        return _this;
    }

    _createClass(Rating, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _props = this.props,
                score = _props.score,
                idRating = _props.idRating,
                rating = _props.rating,
                isVoting = _props.isVoting;

            //paint the start according its score

            if (isVoting) {
                if (!(0, _utilFunctions.isEmpty)(score)) {
                    this.setState({ score: score });
                    this.colorStar(score, idRating);
                }
            }
        }

        /**
         * vote in determinate recipe
         * 
         * @param {object} event 
         * @param {integer} number 
         * @param {string} idRating 
         * @param {string} idRecipe 
         * @memberof Rating
         */

    }, {
        key: 'vote',
        value: function vote(event, number, idRating, idRecipe) {

            this.colorStar(number, idRating);
            this.removeSimulateColor(null, 5, idRating);

            this.setState({ rate: number });

            //update store data
            this.props.voteRecipe(idRecipe, number + 1);
        }

        //----------------- DOM Manipulation functions ----------------- 

        /**
         * paint the starts
         * simulating new vote
         * 
         * @param {object} event 
         * @param {integer} number 
         * @param {string} idRating 
         * @memberof Rating
         */

    }, {
        key: 'simulateVote',
        value: function simulateVote(event, number, idRating) {

            //get the parent star div
            var parentStars = document.querySelector('div[idrating=\'' + idRating + '\']');

            //add the colored class to the parent star 
            //which mouse is hovering and the others before its  
            parentStars.childNodes.forEach(function (starContent, index) {
                if (index <= number) starContent.classList.add("colored");
            });
        }

        /**
         * remove the paint
         * added by simulate vote
         * 
         * @param {object} event 
         * @param {integer} number 
         * @param {string} idRating 
         * @memberof Rating
         */

    }, {
        key: 'removeSimulateColor',
        value: function removeSimulateColor(event, number, idRating) {

            //get the parent star div
            var parentStars = document.querySelector('div[idrating=\'' + idRating + '\']');

            //remove the colored class to the all parents stars
            parentStars.childNodes.forEach(function (starContent, index) {
                starContent.classList.remove("colored");
            });
        }

        /**
         * paint the stars
         * with the same number of votes
         * 
         * @param {integer} number 
         * @param {string} idRating 
         * @memberof Rating
         */

    }, {
        key: 'colorStar',
        value: function colorStar(number, idRating) {

            //get the parent star div
            var parentStars = document.querySelector('div[idrating=\'' + idRating + '\']');

            //add the voted class to the corrects parents stars 
            parentStars.childNodes.forEach(function (starContent, index) {
                if (index <= number) starContent.classList.add("voted");else starContent.classList.remove("voted");
            });
        }

        //----------------- render functions ----------------- 

        /**
         * render the vote area
         * with some number of stars
         * 
         * @param {string} idRating 
         * @param {string} idRecipe 
         * @param {number} starsNumber 
         * @returns {jsx}
         * @memberof Rating
         */

    }, {
        key: 'renderVoteArea',
        value: function renderVoteArea(idRating, idRecipe) {
            var _this2 = this;

            var starsNumber = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 5;

            return (
                //render stars containers 
                [].concat(_toConsumableArray(Array(starsNumber))).map(function (x, index) {
                    return _react2.default.createElement(
                        'div',
                        {
                            key: index,
                            onMouseOver: function onMouseOver() {
                                return _this2.simulateVote(event, index, idRating);
                            },
                            onMouseLeave: function onMouseLeave() {
                                return _this2.removeSimulateColor(event, index, idRating);
                            },
                            onClick: function onClick() {
                                return _this2.vote(event, index, idRating, idRecipe);
                            }
                        },
                        _react2.default.createElement(_star2.default, { key: index, idrating: index, star: 'star' })
                    );
                })
            );
        }

        /**
         * render a non interactive
         * stars which shows score
         * 
         * @param {string} idRating 
         * @param {number} rating 
         * @param {number} starsNumber 
         * @returns {jsx}
         * @memberof Rating
         */

    }, {
        key: 'renderAverageScore',
        value: function renderAverageScore(idRating, rating) {
            var starsNumber = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 5;

            return (
                //render stars containers 
                [].concat(_toConsumableArray(Array(starsNumber))).map(function (x, index) {
                    return _react2.default.createElement(
                        'div',
                        { key: index, className: 'voted average-score' },

                        //logic to show half full and empty stars
                        index + 1 > rating && rating > index ? _react2.default.createElement(_starHalfEmpty2.default, { key: index, idrating: index, star: 'star' }) : rating >= index + 1 ? _react2.default.createElement(_star2.default, { key: index, idrating: index, star: 'star' }) : _react2.default.createElement(_starO2.default, { key: index, idrating: index, star: 'star' })
                    );
                })
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _props2 = this.props,
                idRating = _props2.idRating,
                idRecipe = _props2.idRecipe,
                classes = _props2.classes,
                isVoting = _props2.isVoting,
                rating = _props2.rating;


            return _react2.default.createElement(
                'div',
                { idrating: idRating, className: classes.rating },
                isVoting ? this.renderVoteArea(idRating, idRecipe) : this.renderAverageScore(idRating, rating)
            );
        }
    }]);

    return Rating;
}(_react.Component);

Rating.propTypes = {
    idRating: _propTypes2.default.string.isRequired,
    idRecipe: _propTypes2.default.string.isRequired,
    isVoting: _propTypes2.default.bool.isRequired,
    rating: _propTypes2.default.number.isRequired,
    score: _propTypes2.default.number,
    classes: _propTypes2.default.object
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
        voteRecipe: function voteRecipe(id, vote) {
            return dispatch((0, _index.voteRecipe)(id, vote));
        }
    };
};

exports.default = (0, _reactJss2.default)(_RatingStyles.styles)((0, _reactRedux.connect)(null, mapDispatchToProps)(Rating));

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("react-router-config");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Login = __webpack_require__(22);

var _Login2 = _interopRequireDefault(_Login);

var _Recipes = __webpack_require__(29);

var _Recipes2 = _interopRequireDefault(_Recipes);

var _NotFound = __webpack_require__(56);

var _NotFound2 = _interopRequireDefault(_NotFound);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = [_extends({}, _Login2.default, {
    path: '/',
    exact: true
}), _extends({}, _Recipes2.default, {
    path: '/recipes',
    exact: true
}), _extends({}, _NotFound2.default)];

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Table = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utilFunctions = __webpack_require__(3);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _TableStyles = __webpack_require__(41);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless Component
 * return Table base
 * 
 * @param {object} props 
 * @returns 
 */
var Table = exports.Table = function Table(props) {

    /**
     * render table lines
     * 
     * @param {array} tableLines 
     * @returns {jsx}
     */
    var renderTableLines = function renderTableLines(tableLines) {
        return tableLines.map(function (line, index) {
            return _react2.default.createElement(
                'div',
                { key: index, className: 'line-table' },
                _react2.default.createElement(
                    'span',
                    { key: Object.keys(line)[0] },
                    Object.keys(line)[0]
                ),
                _react2.default.createElement(
                    'span',
                    { key: Object.values(line)[0] },
                    Object.values(line)[0]
                )
            );
        });
    };

    return _react2.default.createElement(
        'div',
        { className: props.classes.table + ' ' + props.anotherClassName },
        (0, _utilFunctions.renderIfExist)(props.title, "h1"),
        _react2.default.createElement(
            'div',
            { className: 'table-body' },
            renderTableLines(props.tableLines)
        )
    );
};

Table.propTypes = {
    title: _propTypes2.default.string,
    tableLines: _propTypes2.default.arrayOf(_propTypes2.default.object),
    text: _propTypes2.default.string
};

exports.default = (0, _reactJss2.default)(_TableStyles.styles)(Table);

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("react-icons/lib/fa/heart");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(18);

var _express = __webpack_require__(19);

var _express2 = _interopRequireDefault(_express);

var _renderer = __webpack_require__(20);

var _renderer2 = _interopRequireDefault(_renderer);

var _reactRouterConfig = __webpack_require__(11);

var _Routes = __webpack_require__(12);

var _Routes2 = _interopRequireDefault(_Routes);

var _createStore = __webpack_require__(59);

var _createStore2 = _interopRequireDefault(_createStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

app.use(_express2.default.static('public'));
app.get('*', function (req, res) {

    var store = (0, _createStore2.default)(req);

    var promises = (0, _reactRouterConfig.matchRoutes)(_Routes2.default, req.path).map(function (_ref) {
        var route = _ref.route;

        return route.loadData ? route.loadData(store) : null;
    }).map(function (promise) {
        if (promise) {
            return new Promise(function (resolve, reject) {
                promise.then(resolve).catch(resolve);
            });
        }
    });

    Promise.all(promises).then(function () {
        var context = {};
        var content = (0, _renderer2.default)(req, store, context);

        if (context.notFound) {
            res.status(404);
        }

        res.send(content);
    });
});

app.listen(process.env.PORT || 3000, function () {
    console.log('Listening on port 3000');
});

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _server = __webpack_require__(21);

var _reactRouterDom = __webpack_require__(10);

var _reactRedux = __webpack_require__(5);

var _reactRouterConfig = __webpack_require__(11);

var _Routes = __webpack_require__(12);

var _Routes2 = _interopRequireDefault(_Routes);

var _serializeJavascript = __webpack_require__(57);

var _serializeJavascript2 = _interopRequireDefault(_serializeJavascript);

var _reactJss = __webpack_require__(2);

var _cssReset = __webpack_require__(58);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var path = _ref.path;
    var store = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var context = arguments[2];


    var sheets = new _reactJss.SheetsRegistry();

    var content = (0, _server.renderToString)(_react2.default.createElement(
        _reactRedux.Provider,
        { store: store },
        _react2.default.createElement(
            _reactJss.JssProvider,
            { registry: sheets },
            _react2.default.createElement(
                _reactRouterDom.StaticRouter,
                {
                    location: path,
                    context: context
                },
                _react2.default.createElement(
                    'div',
                    null,
                    (0, _reactRouterConfig.renderRoutes)(_Routes2.default)
                )
            )
        )
    ));

    return '\n        <html>\n            <head>\n                <style>' + _cssReset.cssReset + '</style>\n                <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">\n                <meta name="viewport" content="width=device-width">\n            </head>\n            <body>\n                <div id="root" >' + content + '</div>\n                <script>\n                    window.INITIAL_STATE = ' + (0, _serializeJavascript2.default)(store.getState()) + '\n                </script>\n            </body>\n            <script src="bundle.js" ></script>\n        </html>\n    ';
};

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Login = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _LoginForm = __webpack_require__(23);

var _LoginForm2 = _interopRequireDefault(_LoginForm);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _LoginStyles = __webpack_require__(28);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Login Page
 * 
 * @param {object} props 
 * @returns 
 */
var Login = exports.Login = function Login(props) {

    return _react2.default.createElement(
        'div',
        { className: props.classes.login },
        _react2.default.createElement(_LoginForm2.default, { className: 'login' })
    );
};

Login.propTypes = {
    classes: _propTypes2.default.object.isRequired
};

exports.default = {
    component: (0, _reactJss2.default)(_LoginStyles.styles)(Login)
};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.LoginForm = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _FormBase = __webpack_require__(24);

var _FormBase2 = _interopRequireDefault(_FormBase);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _reactRouterDom = __webpack_require__(10);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * React class component
 * return Form Login
 * 
 * @class LoginForm
 * @extends {Component}
 * @return {jsx}
 */
var LoginForm = exports.LoginForm = function (_Component) {
    _inherits(LoginForm, _Component);

    function LoginForm() {
        _classCallCheck(this, LoginForm);

        var _this = _possibleConstructorReturn(this, (LoginForm.__proto__ || Object.getPrototypeOf(LoginForm)).call(this));

        _this.state = {
            inputItens: {
                email: {
                    label: 'email',
                    value: '',
                    error: {
                        message: null
                    }
                },
                password: {
                    label: 'password',
                    value: '',
                    type: 'password',
                    error: {
                        message: null
                    }
                }
            },
            validationsMessage: {
                required: 'This is required',
                email: 'Invalid email address'
            },
            buttonDisabled: true

            //bind functions
        };_this.handleInputChange = _this.handleInputChange.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        return _this;
    }

    /**
     * fuction verify if 
     * has error on inputs form
     * 
     * @param {object} inputItens 
     * @returns {boolean}
     * @memberof LoginForm
     */


    _createClass(LoginForm, [{
        key: 'hasFormError',
        value: function hasFormError(inputItens) {
            var _this2 = this;

            var key = _lodash2.default.findKey(inputItens, function (_ref) {
                var error = _ref.error,
                    value = _ref.value;

                return error.message !== null || _this2.checkEmptyInput(value);
            });

            return key !== undefined;
        }

        /**
         * update input value and
         * handle possibles errors
         * 
         * @param {string} value 
         * @param {object} field 
         * @returns {object}
         * @memberof LoginForm
         */

    }, {
        key: 'updateInputObject',
        value: function updateInputObject(value, field) {
            var checkEmptyInput = this.checkEmptyInput,
                checkEmailValidation = this.checkEmailValidation;
            var _state = this.state,
                inputItens = _state.inputItens,
                validationsMessage = _state.validationsMessage,
                password = inputItens.password,
                email = inputItens.email;


            inputItens[field].value = value;

            switch (field) {

                case 'email':

                    email.error.message = checkEmptyInput(value) ? validationsMessage.required : !checkEmailValidation(value) ? validationsMessage.email : null;

                    return inputItens;

                case 'password':

                    password.error.message = checkEmptyInput(value) ? validationsMessage.required : null;

                    return inputItens;

                default:
                    return inputItens;
            }
        }

        //----------------- handle form functions ----------------- 

        /**
         * handle the form 
         * submition 
         * 
         * @param {any} event 
         * @memberof LoginForm
         */

    }, {
        key: 'handleSubmit',
        value: function handleSubmit(event) {
            event.preventDefault();

            //send to recipes page
            this.props.history.push('/recipes');
        }

        /**
        * function deal with 
        * input value changes
        * 
        * @param {object} event 
        * @memberof LoginForm
        */

    }, {
        key: 'handleInputChange',
        value: function handleInputChange(event) {
            var _event$target = event.target,
                value = _event$target.value,
                name = _event$target.name;
            var _state2 = this.state,
                inputItens = _state2.inputItens,
                buttonDisabled = _state2.buttonDisabled;

            //update input values and validate it

            inputItens = this.updateInputObject(value, name);

            //enable button if the form has no error 
            buttonDisabled = this.hasFormError(inputItens);

            this.setState({ inputItens: inputItens, buttonDisabled: buttonDisabled });
        }

        //----------------- input validation functions ----------------- 

        /**
         * return if value is empty
         * 
         * @param {string} value 
         * @returns {boolean}
         * @memberof LoginForm
         */

    }, {
        key: 'checkEmptyInput',
        value: function checkEmptyInput(value) {
            return value.length === 0;
        }

        /**
         * check if email is valid
         * 
         * @param {string} email 
         * @returns {boolean}
         * @memberof LoginForm
         */

    }, {
        key: 'checkEmailValidation',
        value: function checkEmailValidation(email) {
            var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return regex.test(email);
        }
    }, {
        key: 'render',
        value: function render() {
            var _state3 = this.state,
                inputItens = _state3.inputItens,
                buttonDisabled = _state3.buttonDisabled;


            return _react2.default.createElement(_FormBase2.default, {
                formTitle: 'Log in',
                fatherClasses: 'login-form',
                inputItens: inputItens,
                handleChange: this.handleInputChange,
                handleSubmit: this.handleSubmit,
                submitButton: { text: 'LOG IN', disabled: buttonDisabled }
            });
        }
    }]);

    return LoginForm;
}(_react.Component);

LoginForm.propTypes = {
    history: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRouterDom.withRouter)(LoginForm);

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.FormBase = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _InputForm = __webpack_require__(25);

var _InputForm2 = _interopRequireDefault(_InputForm);

var _utilFunctions = __webpack_require__(3);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _FormBaseStyle = __webpack_require__(27);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless component
 * return Form base 
 * 
 * @param {object} props 
 * @returns {jsx}
 */
var FormBase = exports.FormBase = function FormBase(props) {

    /**
     * render form Input list 
     * 
     * @param {array} inputItens 
     * @param {function} handleChange 
     * @returns {jsx} inputList
     */
    var renderListInput = function renderListInput(inputItens, handleChange) {
        return _react2.default.createElement(
            'div',
            { className: 'modal-body' },

            //render inputs
            _lodash2.default.map(inputItens, function (inputItem, id) {
                return _react2.default.createElement(_InputForm2.default, _extends({
                    key: id
                }, inputItem, {
                    setValue: handleChange
                }));
            })
        );
    };

    /**
     * render Form Header
     * 
     * @param {string} formTitle 
     * @returns {jsx} 
     */
    var renderFormHeader = function renderFormHeader(formTitle) {
        return _react2.default.createElement(
            'div',
            { className: 'modal-header' },
            _react2.default.createElement(
                'h4',
                { className: 'title' },
                formTitle
            )
        );
    };

    /**
     * render form cancel button
     * 
     * @param {object} cancelButton 
     * @returns {jsx} 
     */
    var renderCancelButton = function renderCancelButton(cancelButton) {
        return _react2.default.createElement(
            'button',
            { type: 'cancel', className: 'cancel-button' },
            cancelButton.textButton ? cancelButton.textButton : "Cancel"
        );
    };

    var classes = props.classes,
        inputItens = props.inputItens,
        handleChange = props.handleChange,
        formTitle = props.formTitle,
        submitButton = props.submitButton,
        cancelButton = props.cancelButton,
        handleSubmit = props.handleSubmit,
        fatherClasses = props.fatherClasses;


    return _react2.default.createElement(
        'form',
        {
            className: classes.form + ' ' + fatherClasses,
            onSubmit: handleSubmit
        },
        !(0, _utilFunctions.isEmpty)(formTitle) && renderFormHeader(formTitle),
        renderListInput(inputItens, handleChange),
        _react2.default.createElement(
            'button',
            {
                type: 'submit',
                disabled: (0, _utilFunctions.isEmpty)(submitButton.disabled) ? false : submitButton.disabled
            },
            submitButton && submitButton.text ? submitButton.text : "Send"
        ),
        cancelButton && renderCancelButton(cancelButton)
    );
};

FormBase.propTypes = {
    handleChange: _propTypes2.default.func.isRequired,
    handleSubmit: _propTypes2.default.func.isRequired,
    classes: _propTypes2.default.object,
    inputItens: _propTypes2.default.object.isRequired,
    formTitle: _propTypes2.default.string,
    submitButtonText: _propTypes2.default.string,
    submitButton: _propTypes2.default.object,
    cancelButton: _propTypes2.default.oneOfType([_propTypes2.default.object, _propTypes2.default.bool])
};

exports.default = (0, _reactJss2.default)(_FormBaseStyle.styles)(FormBase);

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.InputForm = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utilFunctions = __webpack_require__(3);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _InputFormStyle = __webpack_require__(26);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless component
 * return Input form base 
 * 
 * @param {object} props 
 * @returns {jsx}
 */
var InputForm = exports.InputForm = function InputForm(props) {
    var classes = props.classes;


    return _react2.default.createElement(
        'div',
        { className: classes.inputGroup },
        (0, _utilFunctions.renderIfExist)(props.label, "label"),
        _react2.default.createElement('input', {
            type: (0, _utilFunctions.isEmpty)(props.type) ? 'text' : props.type,
            placeholder: props.placeholder,
            onChange: props.setValue,
            value: props.value,
            name: (0, _utilFunctions.isEmpty)(props.label) ? false : props.label
        }),
        (0, _utilFunctions.renderIfExist)(props.error.message, "span", "error")
    );
};

InputForm.propTypes = {
    label: _propTypes2.default.string,
    placeholder: _propTypes2.default.string,
    inputType: _propTypes2.default.string,
    setValue: _propTypes2.default.func.isRequired,
    error: _propTypes2.default.object,
    classes: _propTypes2.default.object,
    value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired
};

exports.default = (0, _reactJss2.default)(_InputFormStyle.styles)(InputForm);

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var fontSize = { fontSize: '13px' };

var styles = exports.styles = {

    '@global': {
        '*': {
            fontFamily: 'Source Sans Pro, sans-serif'
        }
    },

    inputGroup: {
        display: 'flex',
        flexDirection: 'column',

        '& label': {
            extend: [fontSize],
            marginBottom: '3px',
            textTransform: 'capitalize'
        },

        '& input': {

            extend: [fontSize],

            width: '100%',
            height: '31px',
            padding: '6px 12px',
            backgroundColor: '#fff',
            border: '1px solid #ccc',
            borderRadius: '3px',
            fontSize: '13px',

            '&:focus': {
                outlineColor: '#66afe9'
            }
        },

        '& .error': {
            color: '#a94442',
            fontSize: '14px',
            marginTop: '2px'
        }
    }
};

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    form: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth: '525px',
        width: '100%',
        padding: '15px',

        '& .modal-body': {

            '& div': {
                marginBottom: '15px'
            }
        },

        '& .modal-header': {
            padding: '15px 0',

            '& .title': {
                fontSize: '20px'
            }
        },

        "& button[type='submit']": {
            backgroundColor: '#91c11e',
            border: '1px solid #80ab1b',
            color: '#fff',
            fontSize: '14px',
            fontWeight: '600',
            padding: '6px 12px',
            borderRadius: '3px',
            boxSizing: 'border-box',
            fontFamily: 'Montserrat , sans-serif',
            cursor: 'pointer',

            '&:disabled': {
                cursor: 'default',
                opacity: '0.4'
            }
        }
    }
};

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    login: {
        display: 'flex',
        justifyContent: 'center',
        height: '100%'
    }
};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Recipes = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _index = __webpack_require__(7);

var _reactRedux = __webpack_require__(5);

var _RecipesList = __webpack_require__(32);

var _RecipesList2 = _interopRequireDefault(_RecipesList);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _RecipeStyles = __webpack_require__(55);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Recipes Page
 * 
 * @class Recipes
 * @extends {Component}
 */
var Recipes = exports.Recipes = function (_Component) {
    _inherits(Recipes, _Component);

    function Recipes() {
        _classCallCheck(this, Recipes);

        var _this = _possibleConstructorReturn(this, (Recipes.__proto__ || Object.getPrototypeOf(Recipes)).call(this));

        _this.state = {
            cardView: false,
            hasError: false

            //bind functions
        };_this.changeReciepsView = _this.changeReciepsView.bind(_this);
        return _this;
    }

    _createClass(Recipes, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.props.fetchRecipes();
        }
    }, {
        key: 'componentDidCatch',
        value: function componentDidCatch(error, errorLog) {
            this.setState({ hasError: true, errorLog: errorLog });
        }

        /**
         * change recipe visualization
         * 
         * @param {bool} show 
         * @memberof Recipes
         */

    }, {
        key: 'changeReciepsView',
        value: function changeReciepsView(show) {
            this.setState({ cardView: show });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _state = this.state,
                cardView = _state.cardView,
                hasError = _state.hasError;


            if (hasError) {
                _react2.default.createElement(
                    'div',
                    { className: this.props.classes.recipes },
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Error to get Recipe data'
                    )
                );
            }

            return _react2.default.createElement(
                'div',
                { className: this.props.classes.recipes },
                _react2.default.createElement(
                    'div',
                    { className: 'header-recipe' },
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Delicious and Quick Recipes'
                    ),
                    _react2.default.createElement(
                        'h3',
                        null,
                        'Some easy-to-cook meals specially created by our chefs.'
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'button-area' },
                    _react2.default.createElement(
                        'button',
                        {
                            className: 'card-view ' + (cardView ? 'selected' : ''),
                            onClick: function onClick() {
                                return _this2.changeReciepsView(true);
                            }
                        },
                        'Card View'
                    ),
                    _react2.default.createElement(
                        'button',
                        {
                            className: 'list-view ' + (!cardView ? 'selected' : ''),
                            onClick: function onClick() {
                                return _this2.changeReciepsView(false);
                            }
                        },
                        'List View'
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'body-recipe' },
                    _react2.default.createElement(_RecipesList2.default, { recipes: this.props.recipes, cardView: this.state.cardView })
                )
            );
        }
    }]);

    return Recipes;
}(_react.Component);

var mapStateToProps = function mapStateToProps(state) {
    return {
        recipes: state.recipes,
        error: state.error

    };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
        fetchRecipes: function fetchRecipes() {
            return dispatch((0, _index.fetchRecipes)());
        }
    };
};

/**
 * load data in backend
 * 
 * @param {any} store 
 * @returns 
 */
var loadData = function loadData(store) {
    return store.dispatch((0, _index.fetchRecipes)());
};

Recipes.propTypes = {
    recipes: _propTypes2.default.array,
    classes: _propTypes2.default.object.isRequired
};

exports.default = {
    loadData: loadData,
    component: (0, _reactJss2.default)(_RecipeStyles.styles)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(Recipes))
};

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = [{"calories":"516 kcal","carbos":"47 g","country":"GB","deliverable_ingredients":["375g Sweet Potatoes","1 Tsp Paprika","2 Tbsps Parmesan Cheese","1 Lemon","A Few Sprigs Thyme","25g Panko Breadcrumbs","1 Tbsp Butter","2 Cod Fillets","150g Sugar Snap Peas","A Few Sprigs Mint","75ml Sour Cream"],"description":"There’s nothing like the simple things in life - the smell of freshly cut grass, sitting outside on a nice sunny day, spending time with friends and family. Well here is a recipe that delivers simple culinary pleasures - some nice fresh fish with a crispy crust, crunchy potato wedges and some delightfully sweet sugar snap peas flavoured with cooling mint. Slip into something comfortable and relax into a delicious dinner!","difficulty":0,"fats":"8 g","favorites":1,"fibers":"","headline":"with Sweet Potato Wedges and Minted Snap Peas","highlighted":true,"id":"533143aaff604d567f8b4571","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/533143aaff604d567f8b4571.jpg","incompatibilities":null,"ingredients":["375g Sweet Potatoes","1 Tsp Paprika","2 Tbsps Parmesan Cheese","1 Lemon","A Few Sprigs Thyme","25g Panko Breadcrumbs","1 Tbsp Butter","2 Cod Fillets","150g Sugar Snap Peas","A Few Sprigs Mint","75ml Sour Cream"],"keywords":[""],"name":"Crispy Fish Goujons ","products":["family-box"],"proteins":"43 g","rating":null,"ratings":null,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/533143aaff604d567f8b4571.jpg","time":"PT35M","undeliverable_ingredients":[],"user":{"email":"imp@hellofresh.hf","latlng":"51.507351, -0.127758","name":"Tyrion Lannister"},"weeks":["2014-W20"]},{"calories":"397 kcal","carbos":"26 g","country":"GB","deliverable_ingredients":["1 Lemon","1 Fillet Skirt Steak","1 Tsp Ras El Hanout","1 Clove Garlic","1 Spring Onion","1/2 Cup Carrot","1 Red Pepper","150g Couscous","A Handful Baby Spinach Leaves","4 Tbsps Coriander"],"description":"Close your eyes, open up your Ras El Hanout and inhale deeply. You are no longer standing in your kitchen. Around you are the sounds of a bustling market. Robed men sell ornate carpets and a camel nibbles affectionately at your ear.  OK, we’re pretty sure Paul McKenna’s job is safe for now, but get cooking this recipe and take dinnertime on a magic carpet ride to Casablanca! Our tip for this recipe is to take your meat out of the fridge at least 30 minutes before dinner which will allow you to cook it more evenly.","difficulty":0,"fats":"5 g","favorites":13,"fibers":"","headline":"with Spinach and Lemon Couscous","highlighted":true,"id":"53314247ff604d44808b4569","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/53314247ff604d44808b4569.jpg","incompatibilities":null,"ingredients":["1 Lemon","1 Fillet Skirt Steak","1 Tsp Ras El Hanout","1 Clove Garlic","1 Spring Onion","1/2 Cup Carrot","1 Red Pepper","150g Couscous","A Handful Baby Spinach Leaves","4 Tbsps Coriander"],"keywords":[""],"name":"Moroccan Skirt Steak ","products":["classic-box"],"proteins":"31 g","rating":1,"ratings":1,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/53314247ff604d44808b4569.jpg","time":"PT30M","undeliverable_ingredients":[],"user":{"email":"khaleesi@hellofresh.hf","latlng":"40.712784, -74.005941","name":"Daenerys Targaryen"},"weeks":["2014-W25"]},{"calories":"458 kcal","carbos":"29 g","country":"GB","deliverable_ingredients":["2 Fillets Sea Bream","1 Lime","A Few Tomatoes","⅓ Cup Onion","2 Cloves Garlic","1/2 Tsp Ginger","1 Tbsp Fish Sauce","2 Tbsp Coriander","2 Cups New Potatoes"],"description":"World-renowned people generally all have one thing in common: a legacy. For Henry Ford it was the motorcar, for Thomas Edison it was the lightbulb. For our intern Simon, it was this lip-smackingly awesome Sea Bream. Taking the warm crunchiness of potatoes against the fresh southern asian flavours of fish with coriander, ginger and lime, it’s the perfect dish for transporting your tastebuds. Don’t let the smell of the fish sauce throw you - add it gradually to your sauce for a really authentic asian spin!","difficulty":0,"fats":"6 g","favorites":9,"fibers":"","headline":"with Tomato Concasse and Crispy Potatoes","highlighted":true,"id":"53314276ff604d28828b456b","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/53314276ff604d28828b456b.jpg","incompatibilities":null,"ingredients":["2 Fillets Sea Bream","1 Lime","A Few Tomatoes","⅓ Cup Onion","2 Cloves Garlic","1/2 Tsp Ginger","1 Tbsp Fish Sauce","2 Tbsp Coriander","2 Cups New Potatoes"],"keywords":[""],"name":"Simple Sumptuous Sea Bream","products":["classic-box"],"proteins":"29 g","rating":2.5,"ratings":2,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/53314276ff604d28828b456b.jpg","time":"PT35M","undeliverable_ingredients":[],"user":{"email":"arry@hellofresh.hf","latlng":"-33.867487, 151.206990","name":"Arya Stark"},"weeks":["2014-W14"]},{"calories":"717 kcal","carbos":"44 g","country":"GB","deliverable_ingredients":["2 Chicken Breasts","1 Ball Mozzarella","2 Tbsps Pesto","A Handful of New Potatoes","1 Bunches of Rocket","1 Onion"],"description":"Nostalgia is a powerful thing. For some it’s triggered by the smell of freshly cut grass, for others by the sound of a love song from their heady teenage years. For Head Chef Patrick it’s all about Swiss Roll. A firm dinnertime favourite from his primary school years, we still see him go all misty eyed at the mere mention of it. We’re pretty sure that was the inspiration behind this little number. Tonight, prepare to create a little nostalgia that the little ‘uns will remember for years!","difficulty":0,"fats":"10 g","favorites":1,"fibers":"","headline":"with Roasted Rocket Potatoes","highlighted":true,"id":"533143bfff604d44808b456a","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/533143bfff604d44808b456a.jpg","incompatibilities":null,"ingredients":["2 Chicken Breasts","1 Ball Mozzarella","2 Tbsps Pesto","A Handful of New Potatoes","1 Bunches of Rocket","1 Onion"],"keywords":[""],"name":"Mozzarella and Pesto Roulades","products":["family-box"],"proteins":"67 g","rating":null,"ratings":null,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/533143bfff604d44808b456a.jpg","time":"PT35M","undeliverable_ingredients":[],"user":{"email":"bastard@hellofresh.hf","latlng":"52.370216, 4.895168","name":"Jon Snow"},"weeks":["2013-W43"]},{"calories":"751 kcal","carbos":"105 g","country":"GB","deliverable_ingredients":[" 3 Soft Wholemeal Wraps","1 Tin Tomatoes","1 Tsp Mexican Spice","1 Tin Black Beans","1 Avocado","1 Lime","5 Tbsps Coriander","1/2 Cup Red Onion","1 Yellow Pepper"],"description":"Head Chef Patrick doesn’t like fuss. He’s always telling us that the best kind of food is simple, soulful grub that makes you feel loved. That said, every dinner is a chance to practice your presentation skills. Bigger plates are a great way of framing your food and a sprinkle of herbs or a drizzle of olive oil at the end gives everything a bit more pizzazz. For this recipe, we took classic Mexican ingredients and played with the presentation to create something that’s as tasty to the eye as it is to the tongue. Arriba!","difficulty":0,"fats":"4 g","favorites":7,"fibers":"","headline":"with Homemade Guacamole and Black Bean Salsa","highlighted":true,"id":"5331430fff604d557f8b456d","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/5331430fff604d557f8b456d.jpg","incompatibilities":null,"ingredients":[" 3 Soft Wholemeal Wraps","1 Tin Tomatoes","1 Tsp Mexican Spice","1 Tin Black Beans","1 Avocado","1 Lime","5 Tbsps Coriander","1/2 Cup Red Onion","1 Yellow Pepper"],"keywords":[""],"name":"Mexican Tortilla Stack","products":["veggie-box"],"proteins":"35 g","rating":4,"ratings":1,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/5331430fff604d557f8b456d.jpg","time":"PT35M","undeliverable_ingredients":[],"user":{"email":"little.finger@hellofresh.hf","latlng":"52.520007, 13.404954","name":"Petyr Baelish"},"weeks":["2014-W30"]},{"calories":"689 kcal","carbos":"84 g","country":"GB","deliverable_ingredients":["2 Cups Butternut Squash","2 Sprigs Rosemary","100g Wild Rice","5 Tbsps Feta Cheese","2 Tbsps Pine Nuts","2 Cup Broccoli Florets","1 Vegetable Stock Pot"],"description":"We’ve all heard that much overused culinary phrase “fusion food”, but this recipe is fusion of a slightly different kind. With the onset of Winter Patrick has taken some decidedly seasonal squash and the deep, heady scent of rosemary and combined them with the lighter flavour of feta and wild rice. The squash takes a little bit of work with a vegetable peeler but once you’ve tackled and roasted it you’ll never look back. We use any leftovers for butternut squash soup!","difficulty":0,"fats":"8 g","favorites":5,"fibers":"","headline":"with Wild Rice, Feta and Pine Nuts","highlighted":true,"id":"53314328ff604d4d808b456b","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/53314328ff604d4d808b456b.jpg","incompatibilities":null,"ingredients":["2 Cups Butternut Squash","2 Sprigs Rosemary","100g Wild Rice","5 Tbsps Feta Cheese","2 Tbsps Pine Nuts","2 Cup Broccoli Florets","1 Vegetable Stock Pot"],"keywords":[""],"name":"Roasted Rosemary Butternut Squash ","products":["veggie-box"],"proteins":"23 g","rating":null,"ratings":null,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/53314328ff604d4d808b456b.jpg","time":"PT40M","undeliverable_ingredients":[],"user":{"email":"brienne@hellofresh.hf","latlng":"-33.867487, 151.206990","name":"Brienne of Tarth"},"weeks":["2013-W50"]},{"calories":"","carbos":"","country":"GB","deliverable_ingredients":["Fresh Gnocchi","1 Large Bunch of Basil","4 Tbsps Pine Nuts","4 Tbsps Hard Italian Cheese","A Handful of Tenderstem Broccoli","1 Clove Garlic"],"description":"‘Allo Genovese’ simply means ‘in the style of Genoa’, which is the northern Italian city famous for the pesto that you’ll be making tonight. ‘Pesto’ actually comes from the word ‘pestare’, which means to pound or crush, referring to the old fashioned method of making it in a pestle & mortar. If you happen to have a food processor, you can whizz the pesto together in that, or alternatively just chop, chop, chop everything until it is tiny. Andiamo! ","difficulty":0,"fats":"","favorites":5,"fibers":"","headline":"with Toasted Pine Nuts and Tenderstem Broccoli","highlighted":true,"id":"53314343ff604d28828b456c","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/53314343ff604d28828b456c.jpg","incompatibilities":null,"ingredients":["Fresh Gnocchi","1 Large Bunch of Basil","4 Tbsps Pine Nuts","4 Tbsps Hard Italian Cheese","A Handful of Tenderstem Broccoli","1 Clove Garlic"],"keywords":[""],"name":"Gnocchi Allo Genovese","products":["veggie-box"],"proteins":"","rating":null,"ratings":null,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/53314343ff604d28828b456c.jpg","time":"PT25M","undeliverable_ingredients":[],"user":{"email":"stannis@hellofresh.hf","latlng":"52.370216, 4.895168","name":"Stannis Baratheon"},"weeks":["2014-W30"]},{"calories":"790 kcal","carbos":"100 g","country":"GB","deliverable_ingredients":["200g Mince Beef","4 Pork Sausages","1 Carton Tomato Passata","2 Cloves Garlic","1 Bunch Parsley","600ml Milk","200g Polenta ","⅓ Cup Parmesan"],"description":"“Polpetti?!” we hear you say. That’s meatballs to you and me. But meatballs so good you want \r\n\r\nto parade them down the street waving your arms aloft like a passionate Italian grandmother. \r\n\r\nThese little rascals are perfect for your little rascals (both big and small!) as you can get \r\n\r\neveryone involved in rolling them up. Once served, everyone around the table must choose an \r\n\r\nItalian name and act Italian for the rest of dinner time. Andiamo!*","difficulty":0,"fats":"9 g","favorites":1,"fibers":"","headline":"with Creamy Parmesan Polenta","highlighted":true,"id":"53314398ff604d6c7a8b456a","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/53314398ff604d6c7a8b456a.jpg","incompatibilities":null,"ingredients":["200g Mince Beef","4 Pork Sausages","1 Carton Tomato Passata","2 Cloves Garlic","1 Bunch Parsley","600ml Milk","200g Polenta ","⅓ Cup Parmesan"],"keywords":[""],"name":"Herbed Italian Polpetti","products":["family-box"],"proteins":"39 g","rating":null,"ratings":null,"thumb":"https://d3hvwccx09j84u.cloudfront.net/thumb/image/53314398ff604d6c7a8b456a.jpg","time":"PT35M","undeliverable_ingredients":[],"user":{"email":"piggy@hellofresh.hf","latlng":"52.520007, 13.404954","name":"Samwell Tarly"},"weeks":["2012-W01"]},{"calories":"556 kcal","carbos":"62 g","country":"GB","deliverable_ingredients":["Chicken Thighs","Sweet Chilli Sauce","Egg Noodles","Green Beans","Red Onion","Lime","Fish Sauce","Chinese Rice Vinegar","Soy Sauce","Garlic","Spring Onions","Plain Flour"],"description":"Patrick has been working on a theory that the fewer utensils you use to eat a meal, the tastier it’s likely to be. Think about it - everything you eat with only a fork is usually delicious. Dispense with cutlery entirely to use your fingers and suddenly you’re in taste bud paradise. That was the thinking behind this Japanese favourite. A surefire winner in the eateries of Tokyo, pick it up with your fingers and get stuck in! The first person to finish has to shout “Banzaaaiiii”!","difficulty":2,"fats":"4 g","favorites":44,"fibers":"","headline":"and Sweet and Sour Noodles","highlighted":true,"id":"5252b20c301bbf46038b477e","image":"https://d3hvwccx09j84u.cloudfront.net/web/image/5252b20c301bbf46038b477e.jpg","incompatibilities":null,"ingredients":["Chicken Thighs","Sweet Chilli Sauce","Egg Noodles","Green Beans","Red Onion","Lime","Fish Sauce","Chinese Rice Vinegar","Soy Sauce","Garlic","Spring Onions","Plain Flour"],"keywords":[""],"name":"Genki Yakitori with Crispy Red Onions","products":["classic-box"],"proteins":"32 g","rating":3.8,"ratings":8,"thumb":"https://d3hvwccx09j84u.cloudfront.net/web/image/5252b20c301bbf46038b477e.jpg","time":"PT40M","undeliverable_ingredients":[],"user":{"email":"khal@hellofresh.hf","latlng":"48.208174, 16.373819","name":"Khal Drogo"},"weeks":["2013-W28"]}]

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getRecipeData = undefined;

var _constants = __webpack_require__(8);

var _axios = __webpack_require__(13);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getRecipeData = exports.getRecipeData = function getRecipeData() {
    return _axios2.default.get(_constants.URL_API);
};

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.RecipesList = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _RecipeCard = __webpack_require__(33);

var _RecipeCard2 = _interopRequireDefault(_RecipeCard);

var _RecipeDetail = __webpack_require__(39);

var _RecipeDetail2 = _interopRequireDefault(_RecipeDetail);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _RecipeListStyles = __webpack_require__(54);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless component
 * render a Recipes list
 * 
 * @param {object} props 
 * @returns {jsx}
 */
var RecipesList = exports.RecipesList = function RecipesList(props) {

    /**
     * render Recipe cards List
     * 
     * @param {object} recipes 
     * @returns {jsx}
     */
    var renderRecipeCardsList = function renderRecipeCardsList(recipes) {
        return recipes.map(function (recipe, index) {
            return _react2.default.createElement(_RecipeCard2.default, {
                key: index,
                recipe: recipe,
                className: 'recipe-card'
            });
        });
    };

    /**
     * render Recipe Details List
     * 
     * @param {object} recipes 
     * @returns {jsx}
     */
    var renderRecipeDetailList = function renderRecipeDetailList(recipes) {
        return recipes.map(function (recipe, index) {
            return _react2.default.createElement(_RecipeDetail2.default, {
                key: index,
                recipe: recipe,
                className: 'recipe-detail'
            });
        });
    };

    return _react2.default.createElement(
        'div',
        { className: props.classes.recipeList },
        props.cardView ? renderRecipeCardsList(props.recipes) : renderRecipeDetailList(props.recipes)
    );
};

RecipesList.propTypes = {
    recipes: _propTypes2.default.array,
    cardView: _propTypes2.default.bool.isRequired
};

exports.default = (0, _reactJss2.default)(_RecipeListStyles.styles)(RecipesList);

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.RecipeCard = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Rating = __webpack_require__(9);

var _Rating2 = _interopRequireDefault(_Rating);

var _utilFunctions = __webpack_require__(3);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _RecipeCardStyles = __webpack_require__(38);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React Stateless component
 * render a Card which shows
 * recipe informations
 * 
 * @class RecipeCard
 * @extends {Component}
 */
var RecipeCard = exports.RecipeCard = function RecipeCard(props) {
    var _props$recipe = props.recipe,
        thumb = _props$recipe.thumb,
        name = _props$recipe.name,
        headline = _props$recipe.headline,
        calories = _props$recipe.calories,
        time = _props$recipe.time,
        ratings = _props$recipe.ratings,
        id = _props$recipe.id,
        rating = _props$recipe.rating,
        image = _props$recipe.image;


    return _react2.default.createElement(
        'div',
        { className: props.classes.recipeCard },
        _react2.default.createElement(
            'div',
            { className: 'header-content' },
            thumb || image ? _react2.default.createElement('img', { src: thumb, alt: name }) : null
        ),
        _react2.default.createElement(
            'div',
            { className: 'text-container' },
            _react2.default.createElement(
                'div',
                { className: 'body-content' },
                (0, _utilFunctions.renderIfExist)(name, "h3"),
                (0, _utilFunctions.renderIfExist)(headline, "p")
            ),
            _react2.default.createElement(
                'div',
                { className: 'footer-content' },
                _react2.default.createElement(
                    'div',
                    { className: 'cal-time-inf' },
                    (0, _utilFunctions.renderIfExist)(calories, "span"),
                    (0, _utilFunctions.renderIfExist)(time ? (0, _utilFunctions.formatDuration)(time) : null, "span")
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'ratings-info' },
                    _react2.default.createElement(_Rating2.default, {
                        idRating: id + '-average',
                        idRecipe: id,
                        rating: (0, _utilFunctions.isEmpty)(rating) ? 0 : rating,
                        isVoting: false,
                        className: 'Rating'
                    })
                )
            )
        )
    );
};

RecipeCard.propTypes = {
    recipe: _propTypes2.default.object.isRequired,
    classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactJss2.default)(_RecipeCardStyles.styles)(RecipeCard);

/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports = require("react-icons/lib/fa/star");

/***/ }),
/* 35 */
/***/ (function(module, exports) {

module.exports = require("react-icons/lib/fa/star-half-empty");

/***/ }),
/* 36 */
/***/ (function(module, exports) {

module.exports = require("react-icons/lib/fa/star-o");

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    rating: {
        display: 'flex',

        '& div': {

            cursor: 'pointer',

            '& svg': {
                height: '30px',
                width: '30px',
                color: 'rgb(145, 193, 30)',
                opacity: 0.3
            },

            '&.voted': {
                '& svg': {
                    color: 'rgb(145, 193, 30)',
                    opacity: '1'
                }
            },

            '&.colored': {
                '& svg': {
                    color: '#659a41 !important',
                    opacity: '1'
                }
            },

            '&.average-score': {
                cursor: 'default'
            }
        }
    }
};

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var container = {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%'
};

var styles = exports.styles = {
    recipeCard: {
        extend: container,
        maxWidth: '320px',
        width: '100%',
        height: '290px',
        margin: '10px',
        boxSizing: 'border-box',
        backgroundColor: '#fff',
        boxShadow: '0 1px 3px 0 rgba(0,0,0,0.1)',

        '& img': {
            width: '100%',
            height: '62%'
        },

        '& .text-container': {
            extend: container,
            justifyContent: 'space-around',
            padding: '10px 8px 10px 8px',
            boxSizing: 'border-box',

            '& .body-content': {

                '& h3': {
                    fontSize: '16px',
                    fontWeight: '600',
                    margin: '0px',
                    lineHeight: '1.8em !important',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis'
                },

                '& p': {
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    fontSize: '14px',
                    lineHeight: '1.7em',
                    margin: '0px'
                }
            },

            '& .footer-content': {
                extend: container,
                flexDirection: 'row',
                alignItems: 'flex-end',
                justifyContent: 'space-between',
                fontSize: '13px',
                color: '#b0aba3',
                fontWeight: '300',

                '& .cal-time-inf': {
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '127px'
                },

                '& .ratings-info': {
                    '& svg': {
                        color: '#b0aba3',
                        width: '15px',
                        height: '15px'

                    }
                }
            }
        }
    }
};

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.RecipeDetail = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _NutritionTable = __webpack_require__(40);

var _NutritionTable2 = _interopRequireDefault(_NutritionTable);

var _Table = __webpack_require__(14);

var _Table2 = _interopRequireDefault(_Table);

var _IngredientsList = __webpack_require__(43);

var _IngredientsList2 = _interopRequireDefault(_IngredientsList);

var _List = __webpack_require__(6);

var _List2 = _interopRequireDefault(_List);

var _UserInfo = __webpack_require__(45);

var _UserInfo2 = _interopRequireDefault(_UserInfo);

var _Incompatibilities = __webpack_require__(47);

var _Incompatibilities2 = _interopRequireDefault(_Incompatibilities);

var _Favorite = __webpack_require__(48);

var _Favorite2 = _interopRequireDefault(_Favorite);

var _Rating = __webpack_require__(9);

var _Rating2 = _interopRequireDefault(_Rating);

var _GeneralAvaliations = __webpack_require__(51);

var _GeneralAvaliations2 = _interopRequireDefault(_GeneralAvaliations);

var _utilFunctions = __webpack_require__(3);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _recipeDetailsStyles = __webpack_require__(53);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * React stateless component
 * render all recipe information
 * 
 * @param {object} this.props 
 * @returns 
 */
var RecipeDetail = exports.RecipeDetail = function (_Component) {
    _inherits(RecipeDetail, _Component);

    function RecipeDetail() {
        _classCallCheck(this, RecipeDetail);

        var _this = _possibleConstructorReturn(this, (RecipeDetail.__proto__ || Object.getPrototypeOf(RecipeDetail)).call(this));

        _this.state = {
            hasError: false,
            errorLog: ''
        };
        return _this;
    }

    _createClass(RecipeDetail, [{
        key: 'componentDidCatch',
        value: function componentDidCatch(error, errorLog) {
            this.setState({ hasError: true, errorLog: errorLog });
        }

        /**
         * render time to prepare
         * and the dificult level
         * on table form
         * 
         * @param {object} recipe 
         * @returns {jsx}
         */

    }, {
        key: 'renderAditionalInformation',
        value: function renderAditionalInformation(recipe) {
            var time = recipe.time,
                difficulty = recipe.difficulty,
                tableLines = [];


            !(0, _utilFunctions.isEmpty)(time) ? (0, _utilFunctions.addPropertyToArray)((0, _utilFunctions.formatDuration)(time), "Preparation Time", tableLines) : null;

            !(0, _utilFunctions.isEmpty)(difficulty) ? (0, _utilFunctions.addPropertyToArray)('Level ' + difficulty, "Cooking difficulty", tableLines) : null;

            return !_lodash2.default.isEmpty(tableLines) ? _react2.default.createElement(_Table2.default, {
                tableLines: tableLines,
                anotherClassName: this.props.classes.table,
                className: 'time-dificult-table'
            }) : null;
        }

        /**
         * render country origin
         * and week posted
         * on table form
         * 
         * @param {object} recipe 
         * @returns {jsx}
         */

    }, {
        key: 'renderOtherInformations',
        value: function renderOtherInformations(recipe) {
            var country = recipe.country,
                weeks = recipe.weeks,
                tableLines = [];


            !(0, _utilFunctions.isEmpty)(country) ? (0, _utilFunctions.addPropertyToArray)('' + country, "Country", tableLines) : null;

            !_lodash2.default.isEmpty(weeks) ? (0, _utilFunctions.addPropertyToArray)((0, _utilFunctions.formatWeek)(weeks), "Weeks", tableLines) : null;

            return !_lodash2.default.isEmpty(tableLines) ? _react2.default.createElement(_Table2.default, {
                tableLines: tableLines,
                anotherClassName: this.props.classes.table,
                className: 'weeks-country-table'
            }) : null;
        }
    }, {
        key: 'render',
        value: function render() {
            var _props$recipe = this.props.recipe,
                name = _props$recipe.name,
                headline = _props$recipe.headline,
                thumb = _props$recipe.thumb,
                incompatibilities = _props$recipe.incompatibilities,
                description = _props$recipe.description,
                user = _props$recipe.user,
                id = _props$recipe.id,
                highlighted = _props$recipe.highlighted,
                favorites = _props$recipe.favorites,
                rating = _props$recipe.rating,
                ratings = _props$recipe.ratings,
                score = _props$recipe.score;


            if (this.state.error) {
                return _react2.default.createElement(
                    'div',
                    { className: this.props.classes.recipeDetail + ' error' },
                    _react2.default.createElement(
                        'h1',
                        null,
                        'Something went wrong'
                    ),
                    _react2.default.createElement(
                        'pre',
                        null,
                        this.state.errorLog
                    )
                );
            }

            return _react2.default.createElement(
                'div',
                { className: this.props.classes.recipeDetail },
                _react2.default.createElement(
                    'div',
                    { className: 'main-container' },
                    _react2.default.createElement(
                        'div',
                        { className: 'visual-section' },
                        _react2.default.createElement(
                            'div',
                            { className: 'image-section' },
                            _react2.default.createElement('img', { src: thumb, alt: name })
                        ),
                        _react2.default.createElement(_GeneralAvaliations2.default, {
                            favorites: favorites,
                            ratings: (0, _utilFunctions.isEmpty)(ratings) ? 0 : ratings,
                            id: id,
                            rating: rating ? rating : 0,
                            className: 'general-avaliations'
                        })
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'text-section' },
                        _react2.default.createElement(
                            'div',
                            { className: 'top-section' },
                            _react2.default.createElement(
                                'div',
                                { className: 'title-section' },
                                (0, _utilFunctions.renderIfExist)(name, "h1"),
                                (0, _utilFunctions.renderIfExist)(headline, "h3")
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'classification-section' },
                                _react2.default.createElement(
                                    'div',
                                    { className: 'favorite-section' },
                                    _react2.default.createElement(_Favorite2.default, {
                                        highlighted: highlighted,
                                        id: id,
                                        className: 'favorite'
                                    })
                                ),
                                _react2.default.createElement(
                                    'div',
                                    null,
                                    _react2.default.createElement(_Rating2.default, {
                                        rating: (0, _utilFunctions.isEmpty)(rating) ? 0 : rating,
                                        score: score,
                                        isVoting: true,
                                        idRecipe: id,
                                        idRating: id + '-vote',
                                        className: 'rating'
                                    })
                                )
                            )
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'summary-container' },
                            _react2.default.createElement(
                                'div',
                                { className: 'description-recipe' },
                                (0, _utilFunctions.renderIfExist)(description, "p")
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'aditional-informations' },
                                this.renderAditionalInformation(this.props.recipe),
                                this.renderOtherInformations(this.props.recipe)
                            )
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'informations-container' },
                    _react2.default.createElement(
                        'div',
                        { className: 'ingredient-section' },
                        _react2.default.createElement(_IngredientsList2.default, {
                            recipe: this.props.recipe,
                            className: 'ingredient-list'
                        })
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'nutrition-section' },
                        _react2.default.createElement(_NutritionTable2.default, {
                            recipe: this.props.recipe,
                            className: 'nutrition-table'
                        })
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'last-container' },
                    _react2.default.createElement(
                        'div',
                        { className: 'user-container' },
                        _react2.default.createElement(_UserInfo2.default, { user: user, className: 'user-info' })
                    ),
                    incompatibilities && _react2.default.createElement(
                        'div',
                        { className: 'incompatibilities' },
                        _react2.default.createElement(_Incompatibilities2.default, {
                            recipe: this.props.recipe
                        })
                    )
                )
            );
        }
    }]);

    return RecipeDetail;
}(_react.Component);

RecipeDetail.propTypes = {
    recipe: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactJss2.default)(_recipeDetailsStyles.styles)(RecipeDetail);

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.NutritionTable = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Table = __webpack_require__(14);

var _Table2 = _interopRequireDefault(_Table);

var _utilFunctions = __webpack_require__(3);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NutritionTableStyles = __webpack_require__(42);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless component
 * return nutritional information 
 * table about this recipe
 * 
 * @param {object} props 
 * @returns {jsx}
 */
var NutritionTable = exports.NutritionTable = function NutritionTable(props) {

    var tableLines = [],
        title = "Nutrition Value";

    /**
     * populate the tablelines
     * array with nutritional information
     * and its names 
     * 
     * @param {object} recipe 
     */
    var createTableLinesProperty = function createTableLinesProperty(recipe) {
        var calories = recipe.calories,
            fats = recipe.fats,
            carbos = recipe.carbos,
            fibers = recipe.fibers,
            proteins = recipe.proteins,
            nutritionArray = [{ property: calories, name: "calories" }, { property: fats, name: "fats" }, { property: carbos, name: "carbos" }, { property: fibers, name: "fibers" }, { property: proteins, name: "proteins" }];

        //add to tablelines the not null properties

        nutritionArray.forEach(function (item) {
            (0, _utilFunctions.addPropertyToArray)(item.property, item.name, tableLines);
        });
    };

    createTableLinesProperty(props.recipe);

    if (!_lodash2.default.isEmpty(tableLines)) {
        return _react2.default.createElement(_Table2.default, {
            tableLines: tableLines,
            title: title,
            anotherClassName: props.classes.table
        });
    }

    return null;
};

NutritionTable.propTypes = {
    recipe: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactJss2.default)(_NutritionTableStyles.styles)(NutritionTable);

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    table: {
        display: 'flex',
        flexDirection: 'column',

        '& h1': {
            fontSize: '27px'
        },

        '& .table-body': {

            marginTop: '10px',

            '& .line-table': {
                display: 'flex',
                justifyContent: 'space-between',

                '& :first-child': {
                    textTransform: 'capitalize',
                    fontWeight: 'bold',
                    marginBottom: '5px'
                }
            }

        }
    }
};

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    table: {
        boxSizing: 'border-box',
        padding: '5px 20px 10px 20px'
    }
};

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.IngredientsList = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _List = __webpack_require__(6);

var _List2 = _interopRequireDefault(_List);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * React stateless component
 * return ingredients list 
 * and the ingredients not delivered 
 * 
 * @param {object} props 
 * @returns {jsx}
 */
var IngredientsList = exports.IngredientsList = function IngredientsList(props) {
    var _props$recipe = props.recipe,
        ingredients = _props$recipe.ingredients,
        undeliverable_ingredients = _props$recipe.undeliverable_ingredients,
        objectList = {};

    //check if ingredients exist

    if (!_lodash2.default.isEmpty(ingredients)) {

        //create object with ingredient list
        objectList[0] = {
            title: "Ingredient List",
            itens: [].concat(_toConsumableArray(ingredients))

            //add to the object undelivery ingredients if exists
        };if (!_lodash2.default.isEmpty(undeliverable_ingredients)) {

            objectList[1] = {
                title: "Undeliverable Ingredients",
                itens: [].concat(_toConsumableArray(undeliverable_ingredients))
            };
        }

        return _react2.default.createElement(_List2.default, { objectList: objectList });
    }

    return null;
};

IngredientsList.propTypes = {
    recipe: _propTypes2.default.object
};

exports.default = IngredientsList;

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var container = {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    flexWrap: 'wrap'
};

var styles = exports.styles = {
    list: {
        extend: container,
        padding: '5px 15px 10px 47px',
        boxSizing: 'border-box',

        '& .list-item': {

            '&.margin-item': {
                marginTop: '20px'
            },

            '& h3': {
                fontSize: '27px',
                marginBottom: '10px'
            },

            '& ul': {
                listStyle: 'inherit',
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'space-between',

                '& li': {
                    width: '170px',
                    margin: '5px 0',
                    fontSize: '14px'
                }
            }
        }
    }
};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.UserInfo = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _List = __webpack_require__(6);

var _List2 = _interopRequireDefault(_List);

var _utilFunctions = __webpack_require__(3);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _UserInfoStyles = __webpack_require__(46);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless component
 * Render a List with 
 * User information 
 * 
 * @param {object} props 
 * @returns {jsx} 
 */
var UserInfo = exports.UserInfo = function UserInfo(props) {

    /**
     * format a variable in acceptable
     * format to objectList
     * 
     * @param {any} property 
     * @param {any} name 
     * @returns {jsx}
     */
    var formatVar = function formatVar(property, name) {
        return _react2.default.createElement(
            'div',
            { className: 'user-inf' },
            _react2.default.createElement(
                'strong',
                null,
                name + ':'
            ),
            _react2.default.createElement('br', null),
            '' + property
        );
    };

    var _props$user = props.user,
        email = _props$user.email,
        latlng = _props$user.latlng,
        name = _props$user.name,
        objectList = {};

    //populate objectList to send to List Component

    objectList[0] = {
        title: "User Info",
        itens: []
    };

    !(0, _utilFunctions.isEmpty)(latlng) ? objectList[0].itens.push(formatVar(latlng, "coordenates")) : null;
    !(0, _utilFunctions.isEmpty)(name) ? objectList[0].itens.push(formatVar(name, "name")) : null;
    !(0, _utilFunctions.isEmpty)(email) ? objectList[0].itens.push(formatVar(email, "email")) : null;

    if (!_lodash2.default.isEmpty(objectList[0].itens)) {
        return _react2.default.createElement(_List2.default, {
            objectList: objectList,
            anotherClassName: props.classes.list
        });
    }

    return null;
};

UserInfo.propTypes = {
    user: _propTypes2.default.object.isRequired,
    classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactJss2.default)(_UserInfoStyles.styles)(UserInfo);

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    list: {
        '& div': {
            '&.user-inf': {
                textAlign: 'center',

                '& strong': {
                    fontWeight: 'bold',
                    textTransform: 'capitalize'
                }

            }
        }
    }
};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Incompatibilities = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _List = __webpack_require__(6);

var _List2 = _interopRequireDefault(_List);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * React stateless component
 * render list of possible 
 * incompatible foods
 * in recipe
 * 
 * @param {object} props 
 * @returns 
 */
var Incompatibilities = exports.Incompatibilities = function Incompatibilities(props) {
    var incompatibilities = props.recipe.incompatibilities;

    //check if property exists to render

    if (!_lodash2.default.isEmpty(incompatibilities)) {

        var objectList = {};

        objectList[0] = {
            title: "Incompatibilities",
            itens: [].concat(_toConsumableArray(incompatibilities))
        };

        return _react2.default.createElement(_List2.default, { objectList: objectList });
    }

    return null;
};

Incompatibilities.propTypes = {
    recipe: _propTypes2.default.object
};

exports.default = Incompatibilities;

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Favorite = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactRedux = __webpack_require__(5);

var _heartO = __webpack_require__(49);

var _heartO2 = _interopRequireDefault(_heartO);

var _heart = __webpack_require__(15);

var _heart2 = _interopRequireDefault(_heart);

var _actions = __webpack_require__(7);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _FavoriteStyles = __webpack_require__(50);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * React class component
 * deals with favotire,
 * render a icon for it 
 * and handle with its states
 * 
 * @class Favorite
 * @extends {Component}
 * @return {jsx}
 */
var Favorite = exports.Favorite = function (_Component) {
    _inherits(Favorite, _Component);

    function Favorite() {
        _classCallCheck(this, Favorite);

        var _this = _possibleConstructorReturn(this, (Favorite.__proto__ || Object.getPrototypeOf(Favorite)).call(this));

        _this.state = {
            highlighted: false
        };
        return _this;
    }

    _createClass(Favorite, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var highlighted = this.props.highlighted;

            this.setState({ highlighted: highlighted });
        }

        /**
         * change the highlighted state 
         * 
         * @memberof Favorite
         */

    }, {
        key: 'toggleFavorite',
        value: function toggleFavorite() {
            var highlighted = this.state.highlighted;


            this.setState({ highlighted: !highlighted });

            var id = this.props.id;

            //update store data

            this.props.highlight(id, highlighted);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var highlighted = this.state.highlighted,
                favorites = this.props.classes.favorites;


            return _react2.default.createElement(
                'div',
                {
                    className: favorites + ' ' + (highlighted ? 'full' : '') + ' favorite ',
                    onClick: function onClick() {
                        return _this2.toggleFavorite();
                    }
                },

                //render the correct icon according state
                highlighted ? _react2.default.createElement(_heart2.default, null) : _react2.default.createElement(_heartO2.default, null)
            );
        }
    }]);

    return Favorite;
}(_react.Component);

Favorite.propTypes = {
    highlighted: _propTypes2.default.bool.isRequired,
    id: _propTypes2.default.string.isRequired,
    classes: _propTypes2.default.object,
    highlight: _propTypes2.default.func.isRequired
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
        highlight: function highlight(id, value) {
            return dispatch((0, _actions.highlight)(id, value));
        }
    };
};

exports.default = (0, _reactJss2.default)(_FavoriteStyles.styles)((0, _reactRedux.connect)(null, mapDispatchToProps)(Favorite));

/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = require("react-icons/lib/fa/heart-o");

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    favorites: {

        cursor: 'pointer',

        '& svg': {
            width: '30px',
            height: '30px',
            color: 'rgb(145, 193, 30)'
        },

        '&.full': {
            '& svg': {
                color: 'rgb(145, 193, 30)'
            }
        }
    }
};

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.GeneralAvaliations = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Rating = __webpack_require__(9);

var _Rating2 = _interopRequireDefault(_Rating);

var _heart = __webpack_require__(15);

var _heart2 = _interopRequireDefault(_heart);

var _utilFunctions = __webpack_require__(3);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactJss = __webpack_require__(2);

var _reactJss2 = _interopRequireDefault(_reactJss);

var _GeneralAvaliationsStyles = __webpack_require__(52);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * React stateless component
 * return visual elements about general favorites and
 * general rating informations
 * 
 * @param {any} props 
 * @returns {jsx}
 */
var GeneralAvaliations = exports.GeneralAvaliations = function GeneralAvaliations(props) {
    var favorites = props.favorites,
        ratings = props.ratings,
        id = props.id,
        rating = props.rating,
        classes = props.classes;


    return _react2.default.createElement(
        'div',
        { className: classes.generalAv },
        _react2.default.createElement(
            'div',
            { className: 'favorite-area' },
            _react2.default.createElement(_heart2.default, null),
            _react2.default.createElement(
                'span',
                null,
                (0, _utilFunctions.renderNumber)(favorites) + ' ' + (0, _utilFunctions.pluralWord)(favorites, "favorite")
            )
        ),
        _react2.default.createElement(
            'div',
            { className: 'score-area' },
            _react2.default.createElement(_Rating2.default, {
                className: 'Rating',
                idRating: id + '-average',
                idRecipe: id,
                rating: rating,
                isVoting: false
            }),
            _react2.default.createElement(
                'span',
                null,
                (0, _utilFunctions.renderNumber)(ratings) + ' ' + (0, _utilFunctions.pluralWord)(ratings, "vote")
            )
        )
    );
};

GeneralAvaliations.propTypes = {
    favorites: _propTypes2.default.number.isRequired,
    ratings: _propTypes2.default.number.isRequired,
    id: _propTypes2.default.string.isRequired,
    rating: _propTypes2.default.number.isRequired,
    classes: _propTypes2.default.object
};

exports.default = (0, _reactJss2.default)(_GeneralAvaliationsStyles.styles)(GeneralAvaliations);

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var container = {
    display: 'flex'
};

var styles = exports.styles = {
    generalAv: {
        extend: container,
        marginTop: '10px',
        justifyContent: 'space-around',
        width: '100%',

        '& .favorite-area': {
            extend: container,

            '& svg': {
                color: '#000!important'
            }
        },

        '& .score-area': {
            extend: container,

            '& svg': {
                width: '20px',
                height: '20px',
                color: '#000!important'
            }
        },

        '& span': {
            marginLeft: '5px'
        }
    }
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var container = {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    flexWrap: 'wrap'
};

var styles = exports.styles = {
    recipeDetail: {
        extend: container,
        maxWidth: '1000px',
        width: '100%',
        flexDirection: 'row',
        padding: '20px',
        boxSizing: 'border-box',
        border: '1px solid #a7a2a2',
        borderRadius: '13px',
        margin: '5px',

        '& .main-container': {
            extend: container,
            flexDirection: 'row',
            justifyContent: 'space-between',

            '& .visual-section': {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                margin: 'auto',

                '& .image-section': {

                    '& img': {
                        display: 'block',
                        maxWidth: '400px',
                        width: '100%'
                    }
                }
            },

            '& .text-section': {

                extend: container,
                flex: '3',
                paddingLeft: '40px',

                '& .top-section': {
                    extend: container,
                    flexDirection: 'row',
                    justifyContent: 'space-between',

                    '& .title-section': {

                        '& h1': {
                            fontSize: '30px',
                            margin: '10px 0',
                            fontWeight: '600'
                        }

                    },

                    '& .classification-section': {
                        marginTop: '20px',
                        display: 'flex',

                        '& .favorite-section': {
                            marginRight: '10px'
                        }
                    }
                },

                '& .summary-container': {
                    extend: container,
                    justifyContent: 'space-between',

                    marginTop: '20px',

                    '& .description-recipe': {
                        '& p': {
                            textAlign: 'justify'
                        }

                    },

                    '& .aditional-informations': {
                        extend: container,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: '10px'

                    }
                }
            }
        },

        '& .informations-container': {
            extend: container,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: '10px',
            boxSizing: 'border-box',

            '& .ingredient-section': {
                flex: '3',
                marginTop: '10px',
                minWidth: '320px',
                border: '1px solid #e8e8e8',
                borderRadius: '13px'
            },

            '& .nutrition-section': {
                extend: container,
                flex: '1',
                marginTop: '10px',
                boxSizing: 'border-box',
                minWidth: '250px',
                marginLeft: '20px',
                border: '1px solid #e8e8e8',
                borderRadius: '13px'
            }

        },

        '& .last-container': {
            extend: container,
            flexDirection: 'row',

            '& .user-container, .incompatibilities': {
                marginTop: '20px',

                '& > div': {
                    padding: '5px 15px 10px 20px'
                },

                '& ul': {
                    listStyle: 'none'
                }
            }
        }

    },

    '@media (max-width: 800px)': {
        recipeDetail: {
            '& .main-container': {
                '& .text-section': {
                    flex: 'none',
                    paddingLeft: '0'
                }
            },

            '& .informations-container': {

                '& .ingredient-section': {
                    minWidth: '0'
                },

                '& .nutrition-section': {
                    flex: 'none',
                    marginLeft: '0'
                }
            }
        }
    },

    table: {
        width: '220px'
    }
};

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    recipeList: {
        display: 'flex',
        width: '100%',
        maxWidth: '1100px',
        flexWrap: 'wrap',
        justifyContent: 'center'
    }
};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var styles = exports.styles = {
    recipes: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',

        '& .header-recipe': {

            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around',
            height: '80px',
            alignItems: 'center',
            margin: '20px 0',
            padding: '10px 0',

            '& h1': {
                fontSize: '32px',
                lineHeight: '38px',
                color: '#343434',
                fontFamily: 'Montserrat',
                fontWeight: '400',
                letterSpacing: '-0.3px',
                textAlign: 'center',
                marginTop: '10px'
            },

            '& h3': {
                fontSize: '16px',
                textAlign: 'center',
                marginTop: '10px'

            }
        },

        '& .button-area': {

            display: 'flex',
            maxWidth: '400px',
            width: '100%',
            marginBottom: '20px',
            marginTop: '5px',

            '& button': {
                textDecoration: 'none',
                color: 'white',
                width: '100%',
                cursor: 'pointer',
                padding: '6px 20px',
                margin: '0 6px',
                borderRadius: '2px',
                fontSize: '16px',
                fontWeight: '600',

                '&:focus': {
                    outline: 'none'
                },

                '&.card-view': {
                    border: 'solid 2px #91c11e',
                    color: '#91c11e',
                    background: '#fff',

                    '&:hover, &.selected': {
                        background: '#91c11e',
                        color: '#fff'
                    },

                    '& .selected': {
                        cursor: 'default'
                    }

                },

                '&.list-view': {
                    border: 'solid 2px #f17341',
                    color: '#f17341',
                    background: '#fff',

                    '&:hover, &.selected': {
                        background: '#f17341',
                        color: '#fff'
                    },

                    '&.selected': {
                        cursor: 'default'
                    }
                }
            }
        }
    },

    '& *': {
        fontFamily: 'Source Sans Pro'
    },

    '@media (max-width: 500px)': {
        recipes: {
            '& .button-area': {
                marginTop: '15px',
                marginBottom: '10px'
            }
        }
    }
};

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.NotFound = undefined;

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(1);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Not Found Page
 * 
 * @param {object} props 
 * @returns 
 */
var NotFound = exports.NotFound = function NotFound(_ref) {
    var staticContext = _ref.staticContext;

    staticContext.notFound = true;

    var style = {
        textAlign: 'center',
        marginTop: '30px',
        fontSize: '30px'
    };

    return _react2.default.createElement(
        'h1',
        { style: style },
        'Ooops, route not found'
    );
};

NotFound.propTypes = {
    staticContext: _propTypes2.default.object
};

exports.default = {
    component: NotFound
};

/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = require("serialize-javascript");

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/*
YUI 3.18.1 (build f7e7bcb)
Copyright 2014 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

var cssReset = exports.cssReset = "html{color:#000;background:#FFF}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0}table{border-collapse:collapse;border-spacing:0}fieldset,img{border:0}address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal}ol,ul{list-style:none}caption,th{text-align:left}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal}q:before,q:after{content:''}abbr,acronym{border:0;font-variant:normal}sup{vertical-align:text-top}sub{vertical-align:text-bottom}input,textarea,select{font-family:inherit;font-size:inherit;font-weight:inherit;*font-size:100%}legend{color:#000}";

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _redux = __webpack_require__(16);

var _reduxThunk = __webpack_require__(60);

var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

var _axios = __webpack_require__(13);

var _axios2 = _interopRequireDefault(_axios);

var _reducers = __webpack_require__(61);

var _reducers2 = _interopRequireDefault(_reducers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (req) {

    var store = (0, _redux.createStore)(_reducers2.default, {}, (0, _redux.applyMiddleware)(_reduxThunk2.default));

    return store;
};

/***/ }),
/* 60 */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _redux = __webpack_require__(16);

var _recipesReducer = __webpack_require__(62);

var _recipesReducer2 = _interopRequireDefault(_recipesReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _redux.combineReducers)({
    recipes: _recipesReducer2.default
});

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.calculateNewRating = exports.findRecipeKey = undefined;

var _constants = __webpack_require__(8);

var _lodash = __webpack_require__(4);

var _lodash2 = _interopRequireDefault(_lodash);

var _utilFunctions = __webpack_require__(3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

exports.default = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var action = arguments[1];


    var key = void 0;

    switch (action.type) {

        case _constants.FETCH_RECIPES:
            return action.payload.data;

        case _constants.TOGGLE_FAVORITE:

            key = findRecipeKey(action.id, state);

            //add or remove number of recipes favorites
            state[key].favorites = action.value ? state[key].favorites - 1 : state[key].favorites + 1;

            return [].concat(_toConsumableArray(state));

        case _constants.VOTE_RECIPE:

            key = findRecipeKey(action.id, state);

            //ensures property is a number
            state[key].ratings = (0, _utilFunctions.renderNumber)(state[key].ratings);
            state[key].rating = (0, _utilFunctions.renderNumber)(state[key].rating);

            //if user not voted yet
            if (!state[key].hasOwnProperty('vote')) {
                state[key].ratings += 1;
                state[key].vote = 0;
            }

            state[key].rating = calculateNewRating(state[key].ratings, state[key].rating, action.vote, state[key].vote);

            state[key].vote = action.vote;

            return [].concat(_toConsumableArray(state));

        default:
            return state;
    }
};

/**
 * find index from 
 * recipe object
 * 
 * @param {string} id 
 * @param {object} recipes 
 * @returns {integer} key
 */


var findRecipeKey = exports.findRecipeKey = function findRecipeKey(id, recipes) {
    return _lodash2.default.findKey(recipes, { 'id': id });
};

/**
 * calculate new rating
 * 
 * @param {integer} ratings 
 * @param {float} rating 
 * @param {integer} vote 
 * @param {integer} lastVote 
 * @returns {float} rating
 */
var calculateNewRating = exports.calculateNewRating = function calculateNewRating(ratings, rating, vote, lastVote) {

    if (ratings === 1) {
        return vote;
    }

    var lastAverage = lastVote === 0 ? rating : rating * 2 - lastVote;

    return (lastAverage + vote) / 2;
};

/***/ })
/******/ ]);