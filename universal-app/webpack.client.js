const path = require('path')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base.js')
const OpenBrowserPlugin = require('open-browser-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')


const config = {

    entry: './src/client/client.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    
    devtool: 'inline-source-map',

    plugins: [
        new OpenBrowserPlugin({ url: 'http://localhost:3000' }),
    ],

}

module.exports = merge(baseConfig, config)