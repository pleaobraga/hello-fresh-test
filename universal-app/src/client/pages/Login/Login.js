import React from 'react'
import LoginForm from '../../components/Form/LoginForm/LoginForm'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import { styles } from './styles/LoginStyles'

/**
 * Login Page
 * 
 * @param {object} props 
 * @returns 
 */
export const Login = (props) => {

    return (
        <div className={props.classes.login} >
            <LoginForm className="login" />
        </div>
    )
}

Login.propTypes = {
    classes: PropTypes.object.isRequired
}

export default {
    component: injectSheet(styles)(Login)
}