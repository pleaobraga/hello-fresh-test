import React from 'react'
import { shallow, mount } from 'enzyme'
import { Login } from '../Login'

let props = {    
    classes: {login: {} }
}

describe('Table', () => {

    const login = shallow(<Login {...props} />)

    it('renders properly', () => {
        expect(login).toMatchSnapshot()
    })

    it('should render 1 child', () => {
        expect(login.children().length).toBe(1)
    })

    it('should render login Form', () => {
        expect(login.find("[className='login']").exists()).toBe(true)
    })
})