import React, { Component } from 'react'
import { fetchRecipes } from '../../actions/index'
import { connect } from 'react-redux'
import RecipesList from '../../components/Recipes/RecipesList/RecipesList'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import { styles } from './styles/RecipeStyles'


/**
 * Recipes Page
 * 
 * @class Recipes
 * @extends {Component}
 */
export class Recipes extends Component {


    constructor() {
        super()
        this.state={
            cardView:false,
            hasError: false
        }

        //bind functions
        this.changeReciepsView = this.changeReciepsView.bind(this)
    }

    componentDidMount() {
        this.props.fetchRecipes()
    }

    componentDidCatch(error, errorLog) {
        this.setState({hasError: true, errorLog})
    }

    /**
     * change recipe visualization
     * 
     * @param {bool} show 
     * @memberof Recipes
     */
    changeReciepsView(show) {
        this.setState({cardView: show})
    }
    

    render() {

        let {
            cardView,
            hasError
        } = this.state

        if(hasError) {
            <div className={this.props.classes.recipes} >
                <h1>Error to get Recipe data</h1>
            </div>
        }

        return( 
        <div className={this.props.classes.recipes} >
            <div className="header-recipe" >
                <h1>Delicious and Quick Recipes</h1>
                <h3>Some easy-to-cook meals specially created by our chefs.</h3>
            </div>
            <div className="button-area" >
                <button
                    className={`card-view ${ cardView ? 'selected' : '' }`} 
                    onClick={() => this.changeReciepsView(true)} 
                >
                    Card View
                </button>
                <button 
                    className={`list-view ${ !cardView ? 'selected' : '' }`} 
                    onClick={() => this.changeReciepsView(false)}
                >
                    List View
                </button>
            </div>
            <div className="body-recipe">
                <RecipesList recipes={this.props.recipes} cardView={this.state.cardView} />
            </div>
        </div>)
    }
}

const mapStateToProps = (state) => ({
    recipes: state.recipes,
    error: state.error

})

const mapDispatchToProps = dispatch => ({
    fetchRecipes: () => dispatch(fetchRecipes())
})

/**
 * load data in backend
 * 
 * @param {any} store 
 * @returns 
 */
const loadData = (store) => {
    return store.dispatch(fetchRecipes())
}

Recipes.propTypes = {
    recipes: PropTypes.array,
    classes: PropTypes.object.isRequired
}


export default {
    loadData,
    component: injectSheet(styles)(connect(mapStateToProps, mapDispatchToProps)(Recipes))
}