export const styles = {
    recipes: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',

        '& .header-recipe': {

            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around',
            height: '80px',
            alignItems: 'center',
            margin: '20px 0',
            padding: '10px 0',

            '& h1': {
                fontSize: '32px',
                lineHeight: '38px',
                color: '#343434',
                fontFamily: 'Montserrat',
                fontWeight: '400',
                letterSpacing: '-0.3px',
                textAlign: 'center',
                marginTop: '10px',
            },
    
            '& h3': {
                fontSize: '16px',
                textAlign: 'center',
                marginTop: '10px',

            }
        },

        '& .button-area': {

            display: 'flex',
            maxWidth: '400px',
            width: '100%',
            marginBottom: '20px',
            marginTop: '5px',

            '& button': {
                textDecoration: 'none',
                color: 'white',
                width: '100%',
                cursor: 'pointer',
                padding: '6px 20px',
                margin: '0 6px',
                borderRadius: '2px',
                fontSize: '16px',
                fontWeight: '600',

                '&:focus': {
                    outline: 'none'
                },
                
    
                '&.card-view': {
                      border: 'solid 2px #91c11e',
                      color: '#91c11e',
                      background: '#fff',
    
                    '&:hover, &.selected': {
                        background: '#91c11e',
                        color: '#fff',
                    },
    
                    '& .selected': {
                        cursor: 'default',
                    }   
    
                },
    
                '&.list-view': {
                    border: 'solid 2px #f17341',
                    color: '#f17341',
                    background: '#fff',

                    '&:hover, &.selected': {
                        background: '#f17341',
                        color: '#fff',
                    },

                    '&.selected': {
                        cursor: 'default',
                    },
                }
            }
        }
    },

    '& *': {
        fontFamily: 'Source Sans Pro',
    },

    '@media (max-width: 500px)': {
        recipes: {
            '& .button-area': {
                marginTop: '15px',
                marginBottom: '10px'
            }
        }
    }
}