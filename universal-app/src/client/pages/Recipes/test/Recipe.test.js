import React from 'react'
import { shallow, mount } from 'enzyme'
import { Recipes } from '../Recipes'

const fetchRecipes = jest.fn()

let props = {    
    recipes: [
        {}
    ],
    classes: {recipes: {} }
}

describe('Recipes', () => {

    props.fetchRecipes = fetchRecipes

    const recipes = shallow(<Recipes {...props} />)

    it('renders properly', () => {
        expect(recipes).toMatchSnapshot()
    })

    describe('when card view = false', () => {
        const recipes = shallow(<Recipes {...props} />)

        it('should list view selected', () => {
            expect(recipes.find('.list-view .selected').exists()).toBe(true)
        })

        it('should card view not selected', () => {
            expect(recipes.find('.card-view .selected').exists()).toBe(false)
        })
    })
})