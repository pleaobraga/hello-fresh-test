import React from 'react'
import { shallow, mount } from 'enzyme'
import { NotFound } from '../NotFound'

let props = {    
    staticContext: {}
}

describe('NotFound', () => {

    const notFound = shallow(<NotFound {...props} />)

    it('renders properly', () => {
        expect(notFound).toMatchSnapshot()
    })

    it('should render title', () => {
        expect(notFound.find('h1').exists()).toBe(true)
    })
})