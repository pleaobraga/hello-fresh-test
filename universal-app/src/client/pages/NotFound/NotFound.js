import React from 'react'
import PropTypes from 'prop-types'


/**
 * Not Found Page
 * 
 * @param {object} props 
 * @returns 
 */
export const NotFound = ({ staticContext }) => {
    staticContext.notFound = true

    const style = {
        textAlign: 'center',
        marginTop: '30px',
        fontSize: '30px'
    }

    return <h1 style={ style } >Ooops, route not found</h1>
}

NotFound.propTypes = {
    staticContext: PropTypes.object
}


export default {
    component: NotFound
}