import { combineReducers } from 'redux'
import recipesReducer from './recipesReducer/recipesReducer'

export default combineReducers({ 
    recipes: recipesReducer
})