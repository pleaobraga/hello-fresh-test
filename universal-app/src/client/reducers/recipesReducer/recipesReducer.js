import { FETCH_RECIPES, TOGGLE_FAVORITE, VOTE_RECIPE } from '../../utils/constants'
import _ from 'lodash'
import { renderNumber, isEmpty } from '../../utils/utilFunctions'

export default (state = [], action) => {

    let key

    switch(action.type) {

        case FETCH_RECIPES: 
            return action.payload.data
        
        case TOGGLE_FAVORITE:

            key = findRecipeKey(action.id, state)
            
            //add or remove number of recipes favorites
            state[key].favorites = action.value 
                ? state[key].favorites - 1
                : state[key].favorites + 1
            
            return [...state]
        
        case VOTE_RECIPE:

            key =  findRecipeKey(action.id, state)

            //ensures property is a number
            state[key].ratings = renderNumber(state[key].ratings)
            state[key].rating = renderNumber(state[key].rating)
            
            //if user not voted yet
            if(!(state[key]).hasOwnProperty('vote')) {
                state[key].ratings += 1 
                state[key].vote = 0          
            }

            state[key].rating = calculateNewRating(state[key].ratings, state[key].rating, action.vote ,state[key].vote)
            
            state[key].vote = action.vote
            
            return [...state]
        
        default: 
            return state
    }
}

/**
 * find index from 
 * recipe object
 * 
 * @param {string} id 
 * @param {object} recipes 
 * @returns {integer} key
 */
export const findRecipeKey = (id, recipes) => {
    return _.findKey(recipes, { 'id': id});
}


/**
 * calculate new rating
 * 
 * @param {integer} ratings 
 * @param {float} rating 
 * @param {integer} vote 
 * @param {integer} lastVote 
 * @returns {float} rating
 */
export const calculateNewRating = (ratings, rating, vote, lastVote) => {

    if(ratings === 1) {
        return vote
    }
    
    const lastAverage = lastVote === 0 ? rating : (rating * 2) - lastVote
    
    return ( lastAverage + vote ) / 2
    
}