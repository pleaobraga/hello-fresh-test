import recipeReducer, { calculateNewRating } from '../recipesReducer'
import { FETCH_RECIPES, TOGGLE_FAVORITE, VOTE_RECIPE } from '../../../utils/constants'

describe('recipeReducer', () => {

    describe('BDD Tests', () => {

        describe('when fetch recipe data', () => {

            const payload = {
                data: [
                    {recipe: 'recipe1'},
                    {recipe: 'recipe2'},
                ]
                
            } 
    
            const newState = { type: FETCH_RECIPES, payload} 
    
            it('should return recipe data', () => {
                expect(recipeReducer(undefined, newState )).toEqual(payload.data);
            })
    
        })
    
        describe('when hightlight a recipe', () => {
    
            const state = [
                {ratings: 0, rating: 0, favorites: 2, id: 1},
                {ratings: 2, rating: 3, favorites: 0, id: 2}
            ]
            
    
            it('should remove hightlight', () => {
    
                let finalState = [
                    {ratings: 0, rating: 0, favorites: 1, id: 1},
                    {ratings: 2, rating: 3, favorites: 0, id: 2}
                ]
    
                const newState = { type: TOGGLE_FAVORITE, id: 1, value: true} 
    
                expect(recipeReducer(state, newState )).toEqual(finalState);
            })
    
            it('should add hightlight', () => {
    
                let finalState = [
                    {ratings: 0, rating: 0, favorites: 1, id: 1},
                    {ratings: 2, rating: 3, favorites: 1, id: 2}
                ]
    
                const newState = { type: TOGGLE_FAVORITE, id: 2, value: false} 
    
                expect(recipeReducer(state, newState )).toEqual(finalState);
            })
    
        })
    
        describe('when vote on a recipe', () => {
    
            const state = [
                {ratings: 0, rating: 0, favorites: 2, id: 1},
                {ratings: 2, rating: 3, favorites: 0, id: 2}
            ]
            
    
            it('should create votes and update rating and ratings', () => {
    
                let finalState = [
                    {ratings: 1, rating: 3, favorites: 2, id: 1, vote: 3},
                    {ratings: 2, rating: 3, favorites: 0, id: 2}
                ]
    
                const newState = { type: VOTE_RECIPE, id: 1, vote: 3} 
    
                expect(recipeReducer(state, newState )).toEqual(finalState);
            })
    
            describe('when recipe already has a vote', () => {
    
                const state = [
                    {ratings: 2, rating: 3, favorites: 2, id: 1, vote: 3},
                    {ratings: 2, rating: 3, favorites: 0, id: 2}
                ]
    
                it('should add update votes and update rating', () => {
    
                    let finalState = [
                        {ratings: 2, rating: 2, favorites: 2, id: 1, vote: 1},
                        {ratings: 2, rating: 3, favorites: 0, id: 2}
                    ]
        
                    const newState = { type: VOTE_RECIPE, id: 1, vote: 1} 
        
                    expect(recipeReducer(state, newState )).toEqual(finalState);
                })
            })
    
        })
    })

    describe('TDD tests', () => {

        describe('testing calculateNewRating function', () => {
            let caseTests = [
                { params: [1, 3, 4, 3], expectedValue: 4 },
                { params: [2, 1, 2, 1], expectedValue: 1.5 },
                { params: [3, 1 , 4 , 2], expectedValue: 2 }
            ]
    
            caseTests.forEach(({params, expectedValue}) => {
                it(`ratings: ${params[0]} rating: ${params[1]} vote: ${params[2]} lastVote: ${params[3]} expected: ${expectedValue}`, () => {
                    expect(calculateNewRating(...params)).toBe(expectedValue)
                })
            }) 
        })
    })
})