import React from 'react'
import { shallow, mount } from 'enzyme'
import { LoginForm } from '../LoginForm'

let props = { history: {teste: 'teste'} }

describe('LoginForm', () => {

    const loginForm = shallow(<LoginForm {...props} />)
    const loginFormMount = mount(<LoginForm {...props} />)

    const { validationsMessage } = loginFormMount.state()

    it('renders properly', () => {
        expect(loginForm).toMatchSnapshot()
    })

    describe('behavior Tests', () => {

        beforeEach(() => {
            loginFormMount.state().inputItens.email.value = ''
            loginFormMount.state().inputItens.password.value = ''
        })
    
        describe('email input', () => {
    
            describe('when the user types into the email input', () => {
    
                const value = 'p',
                      name = 'email'
            
                beforeEach(() => {
                    loginFormMount.find("input[name='email']")
                        .simulate('change', { target: { name, value}})  
                })  
        
                describe('types one charactere', () => {
        
                    it('updates the local email value', () => {
                        expect(loginFormMount.state().inputItens.email.value).toEqual(value)
                    })
            
                    it('update the local email error message', () => {
                        expect(loginFormMount.state().inputItens.email.error.message).toEqual(validationsMessage.email)
                    })
            
                    it('should submit button not works ', () => {
                        expect(loginFormMount.find("button[disabled=true]").exists()).toBe(true)
                    })
                })
        
                describe('deletes the content', () => {
    
                    const value = '',
                          name = 'email'
        
                    beforeEach(() => {
                        loginFormMount.find("input[name='email']")
                            .simulate('change', { target: { name, value }})  
                    })  
        
                    it('updates the local email value', () => {
                        expect(loginFormMount.state().inputItens.email.value).toEqual('')
                    })
            
                    it('update the local email error message', () => {
                        expect(loginFormMount.state().inputItens.email.error.message).toEqual(validationsMessage.required)
                    })
            
                    it('should submit button not works ', () => {
                        expect(loginFormMount.find("button[disabled=true]").exists()).toBe(true)
                    })
        
                })
            })
    
             describe('when user type a valid email', () => {
    
                 const value = 'pleao.braga@gmail.com',
                       name = 'email'
                
                 beforeEach(() => {
                     loginFormMount.find("input[name='email']")
                     .simulate('change', { target: { name, value }})  
                 })  
    
                 it('updates the local email value', () => {
                     expect(loginFormMount.state().inputItens.email.value).toEqual(value)
                 })
        
                 it('update the local email error message', () => {
                     expect(loginFormMount.state().inputItens.email.error.message).toEqual(null)
                 })
        
                 it('should submit button not works ', () => {
                     expect(loginFormMount.find("button[disabled=true]").exists()).toBe(true)
                 })
    
             })
        })
    
        
        describe('password input', () => {
    
            describe('when the user types into the password input', () => {
    
                const value = 'p',
                      name = 'password'
    
                beforeEach(() => {
                    loginFormMount.find(`input[name='${name}']`)
                        .simulate('change', { target: { name, value }})  
                })  
    
                describe('types one charactere', () => {
    
                    it('updates password value', () => {
                        expect(loginFormMount.state().inputItens.password.value).toEqual(value)
                    })
        
                    it('update password error message', () => {
                        expect(loginFormMount.state().inputItens.password.error.message).toEqual(null)
                    })
        
                    it('should submit button not works ', () => {
                        expect(loginFormMount.find("button[disabled=true]").exists()).toBe(true)
                    })
                })
    
                describe('deletes the content', () => {
    
                    const value = '',
                          name = 'password'
    
                    beforeEach(() => {
                        loginFormMount.find(`input[name='${name}']`)
                            .simulate('change', { target: { name, value }})  
                    })  
    
                    it('updates password value', () => {
                        expect(loginFormMount.state().inputItens.password.value).toEqual('')
                    })
        
                    it('update password error message', () => {
                        expect(loginFormMount.state().inputItens.password.error.message).toEqual(validationsMessage.required)
                    })
        
                    it('should submit button not works ', () => {
                        expect(loginFormMount.find("button[disabled=true]").exists()).toBe(true)
                    })
    
                })
            })
        })
    
        describe('when user type valid email and valid password', () => {
    
            const emailValue = 'pleao.braga@gail.com',
                  emailName = 'email',
                  passwordValue = 'p',
                  passwordName = 'password'
    
            beforeEach(() => {
                loginFormMount.find(`input[name='${emailName}']`)
                    .simulate('change', { target: { name: emailName, value: emailValue }})  
    
                loginFormMount.find(`input[name='${passwordName}']`)
                    .simulate('change', { target: { name: passwordName, value: passwordValue }})  
            })  
    
    
            it('updates email value', () => {
                expect(loginFormMount.state().inputItens.email.value).toEqual(emailValue)
            })
    
            it('updates password value', () => {
                expect(loginFormMount.state().inputItens.password.value).toEqual(passwordValue)
            })
    
            it('update email error message', () => {
                expect(loginFormMount.state().inputItens.email.error.message).toEqual(null)
            })
    
            it('update password error message', () => {
                expect(loginFormMount.state().inputItens.password.error.message).toEqual(null)
            })
    
            it('should submit button not works ', () => {
                expect(loginFormMount.find("button[disabled=true]").exists()).toBe(false)
            })  
        })

    })

    describe('unity Tests', () => {

        describe('vality checkEmptyInput function', () => {

            let tests = [
                { testString: 'test', testResult: false},
                { testString: 'pedro', testResult: false},
                { testString: 'Hello Fresh', testResult: false},
                { testString: '3', testResult: false},
                { testString: '', testResult: true},
            ]

            tests.forEach( ({testString, testResult}) => {
                it(`test string ${testString}`, () => {
                    expect(loginForm.instance().checkEmptyInput(testString)).toBe(testResult)
                })
            })

        })

        describe('vality checkEmailValidation function', () => {

            let tests = [
                { testString: 'ple', testResult: false},
                { testString: 'p.g@.br', testResult: false},
                { testString: 'p.g@..br', testResult: false},
                { testString: 'p.g@.com.br', testResult: false},
                { testString: '_@gmail.com.br', testResult: true},
                { testString: '_@gmail..com.br', testResult: false},
                { testString: '_@gmail.com..br', testResult: false},
                { testString: 'leao@gmail.com..br', testResult: false},
                { testString: 'pleao.leao@gmail.com.br', testResult: true},
                { testString: 'pleao_leao@gmail.com.br', testResult: true}
            ]

            tests.forEach( ({testString, testResult}) => {
                it(`test string ${testString}`, () => {
                    expect(loginForm.instance().checkEmailValidation(testString)).toBe(testResult)
                })
            })

        })

    })

})
