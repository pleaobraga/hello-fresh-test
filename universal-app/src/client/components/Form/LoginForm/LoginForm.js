import React, { Component } from 'react'
import FormBase from '../FormBase/FormBase'
import _ from  'lodash'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'


/**
 * React class component
 * return Form Login
 * 
 * @class LoginForm
 * @extends {Component}
 * @return {jsx}
 */
export class LoginForm extends Component {

    constructor() {
        super()
        this.state = {
            inputItens: {
                email: {
                    label: 'email',
                    value: '',
                    error: {
                        message: null
                    }
                },
                password: {
                    label: 'password',
                    value: '',
                    type: 'password',
                    error: {
                        message: null
                    }
                }
            },
            validationsMessage: {
                required: 'This is required',
                email: 'Invalid email address'
            },
            buttonDisabled: true
        }

        //bind functions
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    /**
     * fuction verify if 
     * has error on inputs form
     * 
     * @param {object} inputItens 
     * @returns {boolean}
     * @memberof LoginForm
     */
    hasFormError(inputItens) {
        let key = _.findKey(inputItens, ({error, value }) => {
            return error.message !== null 
                || this.checkEmptyInput(value)
        }) 
        
        return key !== undefined
    }


    /**
     * update input value and
     * handle possibles errors
     * 
     * @param {string} value 
     * @param {object} field 
     * @returns {object}
     * @memberof LoginForm
     */
    updateInputObject(value, field) {
        const {
                checkEmptyInput,
                checkEmailValidation,
        } = this

        let { 
            inputItens, 
            validationsMessage 
        } = this.state,
        { 
            password,
            email,
        } = inputItens

        inputItens[field].value = value

        switch(field) {

            case 'email':

                email.error.message = checkEmptyInput(value) 
                    ? validationsMessage.required
                    : !checkEmailValidation(value) 
                    ? validationsMessage.email 
                    : null

                return inputItens

            case 'password':

                password.error.message = checkEmptyInput(value) 
                    ?  validationsMessage.required 
                    :  null
                    
                return inputItens

            default:
                return inputItens
        }
    }

    //----------------- handle form functions ----------------- 

    /**
     * handle the form 
     * submition 
     * 
     * @param {any} event 
     * @memberof LoginForm
     */
    handleSubmit(event) {
        event.preventDefault()

        //send to recipes page
        this.props.history.push('/recipes');
    }

     /**
     * function deal with 
     * input value changes
     * 
     * @param {object} event 
     * @memberof LoginForm
     */
    handleInputChange (event) {
        const {
            value,
            name
        } = event.target
        
        let { inputItens, buttonDisabled } = this.state

        //update input values and validate it
        inputItens = this.updateInputObject(value, name)  

        //enable button if the form has no error 
        buttonDisabled = this.hasFormError(inputItens)
        
        this.setState({inputItens, buttonDisabled});
    }

    //----------------- input validation functions ----------------- 

    /**
     * return if value is empty
     * 
     * @param {string} value 
     * @returns {boolean}
     * @memberof LoginForm
     */
    checkEmptyInput(value) {
        return value.length === 0
    }


    /**
     * check if email is valid
     * 
     * @param {string} email 
     * @returns {boolean}
     * @memberof LoginForm
     */
    checkEmailValidation(email) {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return regex.test(email)
    }

    render() {

        let {
            inputItens,
            buttonDisabled
        } = this.state

        return (
            <FormBase
                formTitle="Log in"
                fatherClasses='login-form'
                inputItens={inputItens}
                handleChange={this.handleInputChange}
                handleSubmit={this.handleSubmit}
                submitButton={{text: 'LOG IN', disabled: buttonDisabled}}
            />
        )
    }
}

LoginForm.propTypes = {
    history: PropTypes.object.isRequired
}


export default withRouter(LoginForm)