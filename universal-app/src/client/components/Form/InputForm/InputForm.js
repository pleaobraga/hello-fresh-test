import React from 'react'
import { renderIfExist, isEmpty } from '../../../utils/utilFunctions'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/InputFormStyle'


/**
 * React stateless component
 * return Input form base 
 * 
 * @param {object} props 
 * @returns {jsx}
 */
export const InputForm = (props) => {

    let { classes } = props

    return (
        <div className={classes.inputGroup}  >
            { renderIfExist(props.label, "label") }
            <input 
                type={ isEmpty(props.type) ? 'text' : props.type  } 
                placeholder={props.placeholder}
                onChange={props.setValue} 
                value={props.value}
                name={isEmpty(props.label) ? false : props.label }
            />
            { renderIfExist(props.error.message, "span", "error") }
        </div>
    )
}

InputForm.propTypes = {
    label: PropTypes.string,
    placeholder: PropTypes.string,
    inputType: PropTypes.string,
    setValue: PropTypes.func.isRequired,
    error: PropTypes.object,
    classes: PropTypes.object,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
};  

export default injectSheet(styles)(InputForm)