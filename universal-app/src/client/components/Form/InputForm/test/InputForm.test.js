import React from 'react'
import { shallow } from 'enzyme'
import { InputForm } from '../InputForm'

const handleChange = () => {}

let props = {
    label: 'password',
    value: '',
    type: 'password',
    error: {
        message: 'teste'
    },
    classes: {inputGroup: {}},
    setValue: handleChange
}

describe('InputForm', () => {

    const inputForm = shallow(<InputForm {...props} />)

    it('renders properly', () => {
        expect(inputForm).toMatchSnapshot();
    });

    describe('when pass all properties', () => {

        it('should displays the label', () => {
            expect(inputForm.find('label').exists()).toBe(true)
        })

        it('check the label text', () => {
            expect(inputForm.find('label').text()).toEqual('password')
        })

        it('should render the input', () => {
            expect(inputForm.find('input').exists()).toBe(true)
        })

        it('check input type', () => {
            expect(inputForm.find("input[type='password']").exists()).toBe(true)
        })

        it('check input value', () => {
            expect(inputForm.find("input[type='password']").text()).toEqual('')
        })

        it('check input name', () => {
            expect(inputForm.find("input[name='password']").exists()).toBe(true)
        })

        it('should render the error message', () => {
            expect(inputForm.find('.error').exists()).toBe(true)
        })

        it('check input value', () => {
            expect(inputForm.find(".error").text()).toEqual('teste')
        })
    })

    describe("when there is no error no type and no label", () => {

        props = {
            value: '',
            classes: {inputGroup: {}},
            error: {},
            setValue: handleChange
        }

        const inputForm = shallow(<InputForm {...props} />)

        it('should not render the error message', () => {
            expect(inputForm.find('.error').exists()).toBe(false)
        })

        it('should not render the label', () => {
            expect(inputForm.find('label').exists()).toBe(false)
        })

        it('check input type', () => {
            expect(inputForm.find("input[type='text']").exists()).toBe(true)
        })

        it('check input name', () => {
            expect(inputForm.find("input[name=false]").exists()).toBe(true)
        })
    })

    describe("when value has some word", () => {

        props = {
            ...props,
            value: 'test',
        }

        const inputForm = shallow(<InputForm {...props} />)

        it('check input type', () => {
            expect(inputForm.find("input[value='test']").exists()).toBe(true)
        })
    })
})