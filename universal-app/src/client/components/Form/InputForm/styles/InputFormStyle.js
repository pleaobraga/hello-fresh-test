const fontSize = { fontSize: '13px' }

export const styles = {

    '@global': {
        '*': {
            fontFamily: 'Source Sans Pro, sans-serif',
        }   
    },

    inputGroup: {
        display: 'flex',
        flexDirection: 'column', 

        '& label': {
            extend: [fontSize],
            marginBottom: '3px',
            textTransform: 'capitalize'
        },

        '& input': {

            extend:[fontSize],

            width: '100%',
            height: '31px',
            padding: '6px 12px',
            backgroundColor: '#fff',
            border: '1px solid #ccc',
            borderRadius: '3px',
            fontSize: '13px',

            '&:focus': {
                outlineColor: '#66afe9'
            }
        },

        '& .error': {
            color: '#a94442',
            fontSize: '14px',
            marginTop: '2px'
        }
    }
}