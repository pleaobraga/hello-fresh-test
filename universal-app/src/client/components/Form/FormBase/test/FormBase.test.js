import React from 'react'
import { shallow, mount } from 'enzyme'
import { FormBase } from '../FormBase'

let inputItens = {
        email: {
            label: 'email',
            value: '',
            error: {
                message: null
            }
        },
        password: {
            label: 'password',
            value: '',
            type: 'password',
            error: {
                message: null
            }
        }
    }, 
    handleSubmit = (event) => {
        event.preventDefault()   
    },
    handleInputChange = (event) => {
        event.preventDefault()   
    }


let props = {
    formTitle:"Log in",
    fatherClasses:'login-formBase',
    inputItens:inputItens,
    handleChange:handleInputChange,
    handleSubmit:handleSubmit,
    submitButton:{text: 'LOG IN', disabled: true},
    classes:{formBase:{}},
    cancelButton:{ textButton: "cancel post" }
}



describe('FormBase', () => {

    const formBase = shallow(<FormBase {...props} />)

    it('renders properly', () => {
        expect(formBase).toMatchSnapshot();
    });


    describe('when pass all properties', () => {
    
        it('should displays the title', () => {
            expect(formBase.find('.title').text()).toEqual('Log in')
        })
    
        it('should render inputList', () => {
            expect(formBase.find('.modal-body').exists()).toBe(true)
        })

        it('should render cancelButton', () => {
            expect(formBase.find('.cancel-button').exists()).toBe(true)
        })

        it('check cancelButton text', () => {
            expect(formBase.find('.cancel-button').text()).toEqual('cancel post')
        })

        it('should render submitButton', () => {
            expect(formBase.find("button[type='submit']").exists()).toBe(true)
        })

        it('check submitButton text', () => {
            expect(formBase.find("button[type='submit']").text()).toEqual('LOG IN')
        })

        describe('When it mounts', () => {
            
            const mountFormBase = mount(<FormBase {...props} />)

            it('render inputs number correctly 2', () => {
                expect(mountFormBase.find('.modal-body').children().length).toBe(2)
            })

            it('check disabled property', () => {
                expect(mountFormBase.find("button[disabled=true]").exists()).toBe(true);
            })
        })
    }) 

    describe('when missing cancelButton text, submitButton text and 1 inputItem', () => {
    
        inputItens = {
            email: {
                label: 'email',
                value: '',
                error: {
                    message: null
                }
            }
        }
        
        props = {
            ...props, 
            inputItens:inputItens,
            submitButton:{},
            cancelButton:{}
        }

        const formBase = shallow(<FormBase {...props} />)

        it('render cancelButton', () => {
            expect(formBase.find('.cancel-button').exists()).toBe(true)
        })

        it('check cancelButton text', () => {
            expect(formBase.find('.cancel-button').text()).toEqual('Cancel')
        })

        it('render submitButton', () => {
            expect(formBase.find("button[type='submit']").exists()).toBe(true)
        })

        it('check submitButton text', () => {
            expect(formBase.find("button[type='submit']").text()).toEqual('Send')
        })

        describe('When it mounts', () => {

            const mountFormBase = mount(<FormBase {...props} />)

            it('render inputs number correctly 1', () => {
                expect(mountFormBase.find('.modal-body').children().length).toBe(1)
            })

            it('check disabled property', () => {
                expect(mountFormBase.find("button[disabled=false]").exists()).toBe(true);
            })
        })
    }) 

    describe('when missing inputItem and title ', () => {
        
        props = {
            ...props, 
            inputItens: {},
            formTitle: null,
        }

        describe('When it mounts', () => {

            const mountFormBase = mount(<FormBase {...props} />)

            it('render inputs number correctly', () => {
                expect(mountFormBase.find('.modal-body').children().length).toBe(0)
            })

            it('should not displays the title', () => {
                expect(mountFormBase.find('.modal-header').exists()).toBe(false)
            })
        })
    }) 


});