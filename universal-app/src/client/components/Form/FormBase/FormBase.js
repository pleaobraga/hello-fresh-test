import React from 'react'
import InputForm from '../InputForm/InputForm'
import { isEmpty } from '../../../utils/utilFunctions'
import _ from 'lodash'
import { styles } from './styles/FormBaseStyle'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'


/**
 * React stateless component
 * return Form base 
 * 
 * @param {object} props 
 * @returns {jsx}
 */
export const FormBase = (props) => {

    /**
     * render form Input list 
     * 
     * @param {array} inputItens 
     * @param {function} handleChange 
     * @returns {jsx} inputList
     */
    const renderListInput = (inputItens, handleChange) => {
        return (
            <div className="modal-body" >
                {
                    //render inputs
                    _.map(inputItens, (inputItem, id) => {
                        return (
                            <InputForm 
                                key={id} 
                                {...inputItem} 
                                setValue={handleChange}
                            />
                        ) 
                    })
                }
            </div>
        )
    }


    /**
     * render Form Header
     * 
     * @param {string} formTitle 
     * @returns {jsx} 
     */
    const renderFormHeader = (formTitle) => {
        return(
            <div className="modal-header" >
                <h4 className='title' >{formTitle}</h4>
            </div>
        )
    }


    /**
     * render form cancel button
     * 
     * @param {object} cancelButton 
     * @returns {jsx} 
     */
    const renderCancelButton = (cancelButton) => {
        return(
            <button type='cancel' className="cancel-button" >
                {
                    cancelButton.textButton 
                    ? cancelButton.textButton 
                    : "Cancel"
                }
            </button> 
        )
    }


    let { 
        classes,
        inputItens,
        handleChange,
        formTitle,
        submitButton,
        cancelButton,
        handleSubmit,
        fatherClasses
    } = props

    return (
        <form 
            className={`${classes.form} ${fatherClasses}`} 
            onSubmit={handleSubmit} 
        >
            { !isEmpty(formTitle) && renderFormHeader(formTitle) }   
            { renderListInput(inputItens, handleChange) }

            <button 
                type='submit'
                disabled={ isEmpty(submitButton.disabled) ? false : submitButton.disabled  } 
            >
                {
                    submitButton && submitButton.text
                    ? submitButton.text
                    : "Send"
                }
            </button> 

            { cancelButton && renderCancelButton(cancelButton) }
            
        </form>
    )
}

FormBase.propTypes = {
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    classes: PropTypes.object,
    inputItens: PropTypes.object.isRequired,
    formTitle:  PropTypes.string,
    submitButtonText:  PropTypes.string,
    submitButton: PropTypes.object,
    cancelButton: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool,
    ])
}

export default injectSheet(styles)(FormBase)
