export const styles = {
    form: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth: '525px',
        width: '100%', 
        padding: '15px',

        '& .modal-body': {

            '& div': {
                marginBottom: '15px',
            }
        },

        '& .modal-header': {
            padding: '15px 0',

            '& .title': {
                fontSize: '20px' 
            }
        },

        "& button[type='submit']": {
            backgroundColor: '#91c11e',
            border: '1px solid #80ab1b',
            color: '#fff',
            fontSize: '14px',
            fontWeight: '600',
            padding: '6px 12px',
            borderRadius: '3px',
            boxSizing: 'border-box',
            fontFamily: 'Montserrat , sans-serif',
            cursor: 'pointer',

            '&:disabled': {
                cursor: 'default',
                opacity: '0.4'
            }
        }
    }
}