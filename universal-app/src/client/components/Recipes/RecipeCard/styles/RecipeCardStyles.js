const container = {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
}

export const styles = {
    recipeCard: {
        extend: container,
        maxWidth: '320px',
        width:'100%',
        height: '290px',
        margin: '10px',
        boxSizing: 'border-box',
        backgroundColor: '#fff',
        boxShadow: '0 1px 3px 0 rgba(0,0,0,0.1)',

        '& img': {
            width: '100%',
            height: '62%'
        },

        '& .text-container': {
            extend: container,
            justifyContent: 'space-around',
            padding: '10px 8px 10px 8px',
            boxSizing: 'border-box',

            '& .body-content': {

                '& h3': {
                    fontSize: '16px',
                    fontWeight: '600',
                    margin: '0px',
                    lineHeight: '1.8em !important',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis'
                },
        
                '& p': {
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    fontSize: '14px',
                    lineHeight: '1.7em',
                    margin: '0px',
                },
            },

            '& .footer-content': {
                extend: container,
                flexDirection: 'row',
                alignItems: 'flex-end',
                justifyContent: 'space-between',
                fontSize: '13px',
                color: '#b0aba3',
                fontWeight: '300',

                '& .cal-time-inf': {
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '127px'
                },

                '& .ratings-info': {
                    '& svg':{
                        color: '#b0aba3',
                        width: '15px',
                        height: '15px',

                    }
                }
            }   
        },
    }
}
