import React from 'react'
import Rating from '../Rating/Rating'
import { formatDuration, renderIfExist, isEmpty } from '../../../utils/utilFunctions'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/RecipeCardStyles'

/**
 * React Stateless component
 * render a Card which shows
 * recipe informations
 * 
 * @class RecipeCard
 * @extends {Component}
 */
export const RecipeCard = (props) => {

    
    let {
        thumb,
        name,
        headline,
        calories,
        time,
        ratings,
        id,
        rating,
        image
    } = props.recipe
    
    return (
        <div className={props.classes.recipeCard}>
            <div className="header-content">
                { thumb || image ? <img src={thumb} alt={name}/> : null }
            </div>
            <div className='text-container' >
                <div className="body-content" >
                    { renderIfExist(name, "h3") }
                    { renderIfExist(headline, "p") }
                </div>
                <div className="footer-content" >
                    <div className='cal-time-inf' >
                        { renderIfExist(calories, "span") }
                        { renderIfExist( time ? formatDuration(time) : null, "span") }
                    </div>
                    <div className='ratings-info' >
                        <Rating 
                            idRating={`${id}-average`}
                            idRecipe={id}
                            rating={isEmpty(rating) ? 0 : rating}
                            isVoting={false}
                            className='Rating'
                        />
                    </div>
                </div>
            </div>
        </div>
    )
    
}

RecipeCard.propTypes = {
    recipe: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
}

export default injectSheet(styles)(RecipeCard)
