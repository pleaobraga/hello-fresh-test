import React from 'react'
import { shallow, mount } from 'enzyme'
import { RecipeCard } from '../RecipeCard'

const voteRecipe = jest.fn()

let props = {
    recipe: {
        thumb: "image.jpg",
        name: "teste",
        headline: "Some Description",
        calories: "450 Kcal",
        time: "35M",
        ratings: 5,
        id: 1,
        rating: 1
    },
    classes: { recipeCard: {} }
    
}

describe('RecipeCard', () => {

    const recipeCard = shallow(<RecipeCard {...props} />)
    
    it('renders properly', () => {
        expect(recipeCard).toMatchSnapshot()
    })

    describe('when send all properties', () => {

        it('should render name', () => {
            expect(recipeCard.find('.body-content h3').exists()).toBe(true)
        })

        it('should render img', () => {
            expect(recipeCard.find('img').exists()).toBe(true)
        })
        it('should render calories headline', () => {
            expect(recipeCard.find('.body-content p').exists()).toBe(true)
        })
        it('should render caories and time duration', () => {
            expect(recipeCard.find('.cal-time-inf span').length).toBe(2)
        })
        it('should render Rating', () => {
            expect(recipeCard.find('.Rating').exists()).toBe(true)
        })
    })

    describe('when not send all properties', () => {

        let props = {
            recipe: {},
            classes: { recipeCard: {} },
        }

        const recipeCard = shallow(<RecipeCard {...props} />)

        it('should render name', () => {
            expect(recipeCard.find('.body-content h3').exists()).toBe(false)
        })

        it('should render img', () => {
            expect(recipeCard.find('img').exists()).toBe(false)
        })
        it('should render calories headline', () => {
            expect(recipeCard.find('.body-content p').exists()).toBe(false)
        })
        it('should render caories and time duration', () => {
            expect(recipeCard.find('.cal-time-inf span').length).toBe(0)
        })
        it('should render Rating', () => {
            expect(recipeCard.find('.Rating').exists()).toBe(true)
        })
    })
    
})