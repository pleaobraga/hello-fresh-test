import React from 'react'
import { shallow, mount } from 'enzyme'
import { Incompatibilities } from '../Incompatibilities'

let props = {
    recipe: { incompatibilities: [ 'rice', 'egg', 'beans' ] }
}

describe('Incompatibilities', () => {

    const incompatibilities = shallow(<Incompatibilities {...props} />)
    const incompatibilitiesMount = mount(<Incompatibilities {...props} />)

    it('renders properly', () => {
        expect(incompatibilities).toMatchSnapshot()
    })


    it('should render list', () => {
        expect(incompatibilitiesMount.children().length).toBe(1)
    })

    describe('when dont have a incompatibilities itens', () =>{
        
        props.recipe.incompatibilities = []
        const incompatibilitiesMount = mount(<Incompatibilities {...props} />)

        it('should not render ', () => {
            expect(incompatibilitiesMount.children().length).toBe(0)
        })
    })
})