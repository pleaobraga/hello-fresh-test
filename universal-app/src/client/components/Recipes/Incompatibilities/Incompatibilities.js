import React from 'react'
import List from '../List/List'
import _ from 'lodash'
import PropTypes from 'prop-types'

/**
 * React stateless component
 * render list of possible 
 * incompatible foods
 * in recipe
 * 
 * @param {object} props 
 * @returns 
 */
export const Incompatibilities = (props) => {
    
    let {
        incompatibilities
    } = props.recipe
    
    //check if property exists to render
    if(!_.isEmpty(incompatibilities)) {

        let objectList = {}
    
        objectList[0] = {
            title: "Incompatibilities",
            itens: [...incompatibilities]
        }
    
        return(
            <List  objectList={objectList} />
        )
    }

    return null
}

Incompatibilities.propTypes = {
    recipe: PropTypes.object
}

export default Incompatibilities