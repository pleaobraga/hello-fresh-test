const container = {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    flexWrap: 'wrap',
}

export const styles = {
    recipeDetail: {
        extend: container,
        maxWidth: '1000px',
        width: '100%',
        flexDirection: 'row',
        padding: '20px',
        boxSizing: 'border-box',
        border: '1px solid #a7a2a2',
        borderRadius: '13px',
        margin: '5px',

        '& .main-container': {
            extend: container,
            flexDirection: 'row',
            justifyContent: 'space-between',

            '& .visual-section': {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                margin: 'auto',

                '& .image-section':{
                        
                    '& img': {
                        display: 'block',
                        maxWidth: '400px',
                        width: '100%',
                    }
                },
            },

           
            '& .text-section': {

                extend: container,
                flex: '3',
                paddingLeft: '40px',

                '& .top-section': {
                    extend: container,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
    
                    '& .title-section': {
                    
                        '& h1': {
                            fontSize: '30px',
                            margin: '10px 0',
                            fontWeight: '600'
                        }
                        
                    },
        
                    '& .classification-section': {
                        marginTop: '20px',
                        display: 'flex',

                        '& .favorite-section': {
                            marginRight: '10px',
                        },
                    }  
                },
        
                '& .summary-container': {
                    extend: container,
                    justifyContent: 'space-between',
        
                    marginTop: '20px',
        
                    '& .description-recipe':{
                        '& p': {
                            textAlign: 'justify'
                        }
        
                    },
        
                    '& .aditional-informations':{
                        extend: container,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: '10px',

                    },
                },
            }
        },
        

        '& .informations-container': {
            extend: container,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: '10px',
            boxSizing: 'border-box',

            '& .ingredient-section': {
                flex: '3',
                marginTop: '10px',
                minWidth: '320px',
                border: '1px solid #e8e8e8',
                borderRadius: '13px',
            },

            '& .nutrition-section': {
                extend: container,
                flex: '1',
                marginTop: '10px',
                boxSizing: 'border-box',
                minWidth: '250px',
                marginLeft: '20px',
                border: '1px solid #e8e8e8',
                borderRadius: '13px',
            }

        },

        '& .last-container': {
            extend: container,
            flexDirection: 'row',

            '& .user-container, .incompatibilities': {
                marginTop: '20px',
    
                '& > div': {
                    padding: '5px 15px 10px 20px',
                },
    
                '& ul': {
                    listStyle: 'none'
                }
            },
        },
  
    },

    '@media (max-width: 800px)': {
        recipeDetail: {
            '& .main-container': {
                '& .text-section': {
                    flex: 'none',
                    paddingLeft: '0',
                },
            },

            '& .informations-container':{

                '& .ingredient-section': {
                    minWidth: '0',
                },

                '& .nutrition-section':{
                    flex: 'none',
                    marginLeft: '0',
                },
            }
        }
    },

    table: {
        width: '220px'
    }
}