import React, {Component} from 'react'
import NutritionTable from '../NutritionTable/NutritionTable'
import Table from '../Table/Table'
import IngredientsList from '../IngredientsList/IngredientsList'
import List from '../List/List'
import UserInfo from '../UserInfo/UserInfo'
import Incompatibilities from '../Incompatibilities/Incompatibilities'
import Favorite from '../Favorite/Favorite'
import Rating from '../Rating/Rating'
import GeneralAvaliations from '../GeneralAvaliations/GeneralAvaliations'
import { 
    formatDuration, 
    isEmpty, 
    addPropertyToArray,
    formatWeek ,
    renderIfExist
} from '../../../utils/utilFunctions'
import _ from 'lodash'
import { styles } from './styles/recipeDetailsStyles'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'

/**
 * React stateless component
 * render all recipe information
 * 
 * @param {object} this.props 
 * @returns 
 */
export class RecipeDetail extends Component {

    constructor() {
        super()

        this.state = {
            hasError: false,
            errorLog: ''
        }
    }

    
    componentDidCatch(error, errorLog) {
        this.setState({hasError: true, errorLog})
    }

    /**
     * render time to prepare
     * and the dificult level
     * on table form
     * 
     * @param {object} recipe 
     * @returns {jsx}
     */
    renderAditionalInformation(recipe) {
        let{
            time,
            difficulty,
        } = recipe,
        tableLines = []

        !isEmpty(time) 
            ? addPropertyToArray(formatDuration(time), "Preparation Time", tableLines ) 
            : null

        !isEmpty(difficulty) 
            ? addPropertyToArray(`Level ${difficulty}`, "Cooking difficulty",tableLines ) 
            : null 

        return !_.isEmpty(tableLines) 
            ? <Table 
                tableLines={tableLines} 
                anotherClassName={this.props.classes.table} 
                className="time-dificult-table"    
              />
            : null
    }


    /**
     * render country origin
     * and week posted
     * on table form
     * 
     * @param {object} recipe 
     * @returns {jsx}
     */
    renderOtherInformations(recipe) {
        let{
            country,
            weeks,
        } = recipe,
        tableLines = []

        !isEmpty(country) 
            ? addPropertyToArray(`${country}`, "Country", tableLines ) 
            : null

        !_.isEmpty(weeks) 
            ? addPropertyToArray(formatWeek(weeks), "Weeks",tableLines ) 
            : null 

        return !_.isEmpty(tableLines) 
            ? <Table 
                tableLines={tableLines} 
                anotherClassName={this.props.classes.table}
                className="weeks-country-table"     
              />
            : null
     }


    render() {
        let {
            name,
            headline,
            thumb,
            incompatibilities,
            description,
            user,
            id,
            highlighted,
            favorites,
            rating,
            ratings,
            score,
        } = this.props.recipe

        if(this.state.error) {
            return (
                <div className={`${this.props.classes.recipeDetail} error`} >
                    <h1>Something went wrong</h1>
                    <pre>{this.state.errorLog}</pre>
                </div>
            )
        }
    
        return (
            <div className={this.props.classes.recipeDetail} >
                <div className="main-container" >
                    <div className='visual-section' >
                        <div className="image-section" >
                            <img src={thumb} alt={name} />
                        </div>
                        <GeneralAvaliations 
                            favorites={favorites}
                            ratings={isEmpty(ratings) ? 0 : ratings}
                            id={id}
                            rating={rating ? rating : 0}
                            className="general-avaliations"
                        />
                    </div>
                    <div className="text-section" >
                        <div className="top-section">
                            <div className="title-section" >
                                {renderIfExist(name, "h1")}
                                {renderIfExist(headline, "h3")}
                            </div>
                            <div className="classification-section" >
                                <div className="favorite-section" >
                                    <Favorite 
                                        highlighted={highlighted}
                                        id={id} 
                                        className="favorite"
                                    />
                                </div>
                                <div>
                                    <Rating 
                                        rating={isEmpty(rating) ? 0 : rating}
                                        score={score}
                                        isVoting={true}
                                        idRecipe={id}
                                        idRating={`${id}-vote`}
                                        className="rating"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="summary-container" >
                            <div className="description-recipe" >
                                {renderIfExist(description, "p")}
                            </div>
                            <div className="aditional-informations" >
                                {this.renderAditionalInformation(this.props.recipe)}
                                {this.renderOtherInformations(this.props.recipe)}
                            </div>
                        </div>
                    </div>
                </div>
                <div  className="informations-container" >
                    <div className="ingredient-section" >
                        <IngredientsList 
                            recipe={this.props.recipe} 
                            className="ingredient-list" 
                        />
                    </div>
                    <div className="nutrition-section" >
                        <NutritionTable 
                            recipe={this.props.recipe}
                            className="nutrition-table" 
                        />
                    </div>
                </div>
                <div className="last-container" >
                    <div  className="user-container" >
                        <UserInfo user={user} className="user-info" />
                    </div>
                    {
                        incompatibilities && (
                            <div  className="incompatibilities" >
                                <Incompatibilities 
                                    recipe={this.props.recipe}
                                />
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }
}

RecipeDetail.propTypes ={
    recipe: PropTypes.object.isRequired
}

export default injectSheet(styles)(RecipeDetail)