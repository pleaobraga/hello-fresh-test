export const styles= {
    list:{
        '& div': {
            '&.user-inf':{
                textAlign: 'center',

                '& strong':{
                    fontWeight: 'bold',
                    textTransform: 'capitalize'
                },

            }
        }
    }
}