import React from 'react'
import { shallow, mount } from 'enzyme'
import { UserInfo } from '../UserInfo'

let props = {
    user: {
        email: "khaleesi@hellofresh.hf",
        latlng: "40.712784, -74.005941",
        name: "Daenerys Targaryen"
    },
    classes: {list: ""}
}

describe('UserInfo', () => {

    const userInfo = shallow(<UserInfo {...props} />)
    const userInfoMount = mount(<UserInfo {...props} />)

    it('renders properly', () => {
        expect(userInfo).toMatchSnapshot()
    })


    it('should render list', () => {
        expect(userInfoMount.children().length).toBe(1)
    })

    describe('when dont have a user object', () =>{
        
        props.user = {}
        const userInfoMount = mount(<UserInfo {...props} />)

        it('should not render ', () => {
            expect(userInfoMount.children().length).toBe(0)
        })
    })
})