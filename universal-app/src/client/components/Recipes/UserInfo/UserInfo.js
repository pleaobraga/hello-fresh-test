import React from 'react'
import List from '../List/List'
import { isEmpty } from '../../../utils/utilFunctions'
import _ from 'lodash'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/UserInfoStyles'


/**
 * React stateless component
 * Render a List with 
 * User information 
 * 
 * @param {object} props 
 * @returns {jsx} 
 */
export const UserInfo = (props) => {

    /**
     * format a variable in acceptable
     * format to objectList
     * 
     * @param {any} property 
     * @param {any} name 
     * @returns {jsx}
     */
    const formatVar = ( property, name ) => {
        return (
            <div className='user-inf'>
                <strong>{`${name}:`}</strong><br/>
                {`${property}`}
            </div>
        ) 
    }


    let {
        email,
        latlng,
        name
    } = props.user,
        objectList = {}

    //populate objectList to send to List Component
    objectList[0] = {
        title: "User Info",
        itens: []
    }

    !isEmpty(latlng) ? objectList[0].itens.push(formatVar(latlng, "coordenates")) : null 
    !isEmpty(name) ?  objectList[0].itens.push(formatVar(name, "name")) : null 
    !isEmpty(email) ?  objectList[0].itens.push(formatVar(email, "email")) : null


    if(!_.isEmpty(objectList[0].itens)) {
        return(
            <List  
                objectList={objectList} 
                anotherClassName={props.classes.list}
            />
        )
    }
    

    return null

}

UserInfo.propTypes = {
    user: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
}


export default injectSheet(styles)(UserInfo)