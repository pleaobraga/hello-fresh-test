import React, { Component } from 'react'
import { connect } from 'react-redux'
import EmptyHeart from 'react-icons/lib/fa/heart-o'
import FullHeart from 'react-icons/lib/fa/heart'
import { highlight } from '../../../actions'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/FavoriteStyles'
 

/**
 * React class component
 * deals with favotire,
 * render a icon for it 
 * and handle with its states
 * 
 * @class Favorite
 * @extends {Component}
 * @return {jsx}
 */
export class Favorite extends Component {

    constructor() {
        super()
        this.state = {
            highlighted: false
        }
    }

    componentDidMount() {
        const { highlighted } = this.props 
        this.setState({highlighted})
    }

    /**
     * change the highlighted state 
     * 
     * @memberof Favorite
     */
    toggleFavorite() {
        const { highlighted } = this.state
        
        this.setState({highlighted: !highlighted})

        const { id } = this.props 

        //update store data
        this.props.highlight(id, highlighted)
    }


    render() {

        let {
            highlighted
        } = this.state,
        { favorites } = this.props.classes

        return(
            <div 
                className={`${favorites} ${ highlighted ? 'full' : '' } favorite ` } 
                onClick={ () => this.toggleFavorite() }
            >
                {
                    //render the correct icon according state
                    highlighted 
                    ? <FullHeart />
                    :  <EmptyHeart />
                }
            </div>
        )
    }
}

Favorite.propTypes = { 
    highlighted: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    classes: PropTypes.object,
    highlight: PropTypes.func.isRequired
}


const mapDispatchToProps = dispatch => ({
    highlight: (id, value) => dispatch(highlight(id, value))
})


export default injectSheet(styles)(connect(null, mapDispatchToProps)(Favorite))

