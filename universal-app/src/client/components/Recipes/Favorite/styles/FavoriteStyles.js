export const styles = {
    favorites: {

        cursor: 'pointer',

        '& svg':{
            width: '30px',
            height: '30px',
            color: 'rgb(145, 193, 30)'
        },

        '&.full': {
            '& svg':{
                color: 'rgb(145, 193, 30)',
            },
        }
    }
}