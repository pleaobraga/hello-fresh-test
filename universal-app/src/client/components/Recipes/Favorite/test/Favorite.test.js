import React from 'react'
import { shallow, mount } from 'enzyme'
import { Favorite } from '../Favorite'

const highlight = jest.fn()

let props = {
    highlighted: true,
    id: "1",
    classes: {favorites: {}}
}

describe('Favorite', () => {

    props.highlight = highlight

    const favorite = shallow(<Favorite {...props} />)

    it('renders properly', () => {
        expect(favorite).toMatchSnapshot()
    })

    describe(' when render highlighted ', () => {

        const favoriteMount = mount(<Favorite {...props} />)

        beforeEach(() => {
            favoriteMount.state().highlighted = props.highlighted
        })


        it('check highlited state after render', ()  => {
            expect(favoriteMount.state().highlighted).toEqual(props.highlighted)
        })
    
        it('should render FullHeart ', ()  => {
            expect(favoriteMount.find('FaHeart').exists()).toBe(true)
        })

        describe('when toggle', () => {
            
            beforeEach(() => {
                favoriteMount.find('.favorite').simulate('click')
            })

            it('check highlited state after render', ()  => {
                expect(favoriteMount.state().highlighted).toEqual(!props.highlighted)
            })

            it('should call the highlight function', ()  => {
                expect(highlight).toHaveBeenCalled();
            })

            it('should render EmptyHeart', ()  => {
                expect(favoriteMount.find('FaHeartO').exists()).toBe(true)
            })

            describe('when toggle again', () => {

                beforeEach(() => {
                    favoriteMount.find('.favorite').simulate('click')
                })

                it('check highlited state after render', ()  => {
                    expect(favoriteMount.state().highlighted).toEqual(props.highlighted)
                })
            
                it('should render FullHeart ', ()  => {
                    expect(favoriteMount.find('FaHeart').exists()).toBe(true)
                })

                it('should call the highlight function', ()  => {
                    expect(highlight).toHaveBeenCalled();
                })
        
            })

        })
    })

})