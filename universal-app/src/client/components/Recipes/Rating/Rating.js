import React, { Component } from 'react'
import { connect } from 'react-redux'
import { voteRecipe } from '../../../actions/index'
import FullStar from 'react-icons/lib/fa/star'
import HalfFullStar from 'react-icons/lib/fa/star-half-empty'
import EmptyStar from 'react-icons/lib/fa/star-o'
import { isEmpty } from '../../../utils/utilFunctions'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import { styles } from './styles/RatingStyles'


/**
 * React Class Component
 * deals with rating
 * render a icon for it 
 * and handle with its states
 * 
 * @class Rating
 * @extends {Component}
 */
export class Rating extends Component {

    constructor() {
        super()
        this.state = {
            score: 0
        }
    }

    componentDidMount() {
        const { 
            score, 
            idRating, 
            rating, 
            isVoting 
        } = this.props

        //paint the start according its score
        if(isVoting) {
            if(!isEmpty(score)) {
                this.setState({score})
                this.colorStar(score, idRating)
            }
        }
    }

    /**
     * vote in determinate recipe
     * 
     * @param {object} event 
     * @param {integer} number 
     * @param {string} idRating 
     * @param {string} idRecipe 
     * @memberof Rating
     */
    vote(event, number, idRating, idRecipe) {
        
        this.colorStar( number, idRating)
        this.removeSimulateColor(null, 5, idRating)
        
        this.setState({rate: number})

        //update store data
        this.props.voteRecipe(idRecipe, number+1)
    }


    //----------------- DOM Manipulation functions ----------------- 

    /**
     * paint the starts
     * simulating new vote
     * 
     * @param {object} event 
     * @param {integer} number 
     * @param {string} idRating 
     * @memberof Rating
     */
    simulateVote(event, number, idRating) {

        //get the parent star div
        let parentStars = document.querySelector(`div[idrating='${idRating}']`)

        //add the colored class to the parent star 
        //which mouse is hovering and the others before its  
        parentStars.childNodes.forEach((starContent, index) => {
            if(index <= number)
                starContent.classList.add("colored")
        })
    }


    /**
     * remove the paint
     * added by simulate vote
     * 
     * @param {object} event 
     * @param {integer} number 
     * @param {string} idRating 
     * @memberof Rating
     */
    removeSimulateColor(event, number, idRating) {

        //get the parent star div
        let parentStars = document.querySelector(`div[idrating='${idRating}']`)

        //remove the colored class to the all parents stars
        parentStars.childNodes.forEach((starContent, index) => {
            starContent.classList.remove("colored") 
        })
    }


    /**
     * paint the stars
     * with the same number of votes
     * 
     * @param {integer} number 
     * @param {string} idRating 
     * @memberof Rating
     */
    colorStar(number, idRating) {

        //get the parent star div
        let parentStars = document.querySelector(`div[idrating='${idRating}']`)

        //add the voted class to the corrects parents stars 
        parentStars.childNodes.forEach((starContent, index) => {
            if(index <= number)
                starContent.classList.add("voted")
            else 
                starContent.classList.remove("voted")
        })
    }

    
    //----------------- render functions ----------------- 

    /**
     * render the vote area
     * with some number of stars
     * 
     * @param {string} idRating 
     * @param {string} idRecipe 
     * @param {number} starsNumber 
     * @returns {jsx}
     * @memberof Rating
     */
    renderVoteArea(idRating, idRecipe, starsNumber = 5 ) {
        return (
            //render stars containers 
            [...Array(starsNumber)].map((x, index) =>{
                return (
                    <div 
                        key={index} 
                        onMouseOver={() => this.simulateVote(event, index, idRating) } 
                        onMouseLeave={() => this.removeSimulateColor(event, index, idRating) } 
                        onClick={() => this.vote(event, index, idRating, idRecipe)}
                    >
                        <FullStar key={index} idrating={index} star="star" />
                    </div>
                )}
            )
        )
    }


    /**
     * render a non interactive
     * stars which shows score
     * 
     * @param {string} idRating 
     * @param {number} rating 
     * @param {number} starsNumber 
     * @returns {jsx}
     * @memberof Rating
     */
    renderAverageScore(idRating, rating, starsNumber = 5) {
        return (
            //render stars containers 
            [...Array(starsNumber)].map((x, index) =>{
                return (
                    <div key={index}  className="voted average-score">
                        {
                            //logic to show half full and empty stars
                            index+1 > rating && rating > index
                                ? <HalfFullStar key={index} idrating={index} star="star" />
                                : rating >= index+1 
                                ? <FullStar key={index} idrating={index} star="star" />
                                : <EmptyStar key={index} idrating={index} star="star" />
                        }
                    </div>
                )}
            )
        )
    }


    render() {

        let {
            idRating,
            idRecipe,
            classes,
            isVoting,
            rating,
        } = this.props

        return(
            <div idrating={idRating} className={classes.rating} >
                {
                    isVoting 
                    ? this.renderVoteArea(idRating, idRecipe)
                    : this.renderAverageScore(idRating, rating)
                }
            </div>
        )
    }
} 

Rating.propTypes = {
    idRating: PropTypes.string.isRequired,
    idRecipe: PropTypes.string.isRequired,
    isVoting: PropTypes.bool.isRequired,
    rating: PropTypes.number.isRequired,
    score: PropTypes.number,
    classes: PropTypes.object
}


const mapDispatchToProps = dispatch => ({
    voteRecipe: (id, vote) => dispatch(voteRecipe(id, vote))
})


export default injectSheet(styles)(connect(null, mapDispatchToProps)(Rating))