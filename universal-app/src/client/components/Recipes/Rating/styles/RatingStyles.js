export const styles = {
    rating: {
        display: 'flex',

        '& div': {  

            cursor: 'pointer',

            '& svg': {
                height: '30px',
                width: '30px',
                color: 'rgb(145, 193, 30)',
                opacity: 0.3,
            },

            '&.voted': {
                '& svg': {
                    color: 'rgb(145, 193, 30)',
                    opacity: '1',    
                }
            },

            '&.colored': {
                '& svg': {
                    color: '#659a41 !important',
                    opacity: '1',
                }
            },

            '&.average-score':{
                cursor: 'default',
            }
        }
    }
}