import React from 'react'
import { shallow, mount } from 'enzyme'
import { Rating } from '../Rating'

const voteRecipe = jest.fn()

let props = {
    idRating: "1",
    idRecipe: "2",
    isVoting: false,
    rating: 2,
    score: null,
    classes: {rating: {}}
}

describe('Rating', () => {

    props.voteRecipe = voteRecipe

    const rating = shallow(<Rating {...props} />)


    it('renders properly', () => {
        expect(rating).toMatchSnapshot()
    })

    describe('when rating is to show ratings', () => {

        it('should render 5 starts', () => {
            expect(rating.find('.average-score').length).toBe(5)
        })

        describe('when rating = 2', () => {
        
            it('should render 2 full stars', () => {
                expect(rating.find('FaStar').length).toBe(2)
            })
    
            it('should render 3 empty stars', () => {
                expect(rating.find('FaStarO').length).toBe(3)
            })
        })

        describe('when rating = 2.5', () => {

            props.rating = 2.5

            const rating = shallow(<Rating {...props} />)

            it('should render 2 full stars', () => {
                expect(rating.find('FaStar').length).toBe(2)
            })

            it('should render 1 half empty star', () => {
                expect(rating.find('FaStarHalfEmpty').length).toBe(1)
            })
    
            it('should render 2 empty stars', () => {
                expect(rating.find('FaStarO').length).toBe(2)
            })
        })

    })

    describe('when rating is to vote', () => {

        props.isVoting = true
        
        const rating = shallow(<Rating {...props} />)

        it('should render 5 starts', () => {
            expect(rating.find('FaStar').length).toBe(5)
        })

        it('should render 0 starts voted', () => {
            expect(rating.find('.voted').length).toBe(0)        
        })
    })
})