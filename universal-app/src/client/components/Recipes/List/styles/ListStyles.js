const container = {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    flexWrap: 'wrap',
}

export const styles = {
    list: {
        extend: container,
        padding: '5px 15px 10px 47px',
        boxSizing: 'border-box',

        '& .list-item': {

            '&.margin-item': {
                marginTop: '20px',
            },
            
            '& h3': {
                fontSize: '27px',
                marginBottom: '10px',
            },
    
            '& ul': {
                listStyle: 'inherit',
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
    
                '& li': {
                    width: '170px',
                    margin: '5px 0',
                    fontSize: '14px',
                }
            }
        } 
    }
}