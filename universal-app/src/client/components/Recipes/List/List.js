import React from 'react'
import _ from   'lodash'
import { renderIfExist } from  '../../../utils/utilFunctions'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/ListStyles'


/**
 * React stateless Component
 * return List base
 * 
 * @param {object} props 
 * @returns {jsx}
 */
export const List = (props) => {


    /**
     * render lists  
     * 
     * @param {object} objectList 
     * @returns {jsx}
     */
    const renderLists = (objectList) => {
        return _.map(props.objectList, (list, index) => {
            return(
                <div key={index} className={` list-item ${index > 0 ? 'margin-item' : ''}`} >
                    {renderIfExist(list.title, "h3")}
                    <ul>
                        { renderItensList(list, index) }
                    </ul>
                </div>
            )
        })
    }


    /**
     * render itens list
     * 
     * @param {object} list 
     * @param {integer} index 
     * @returns 
     */
    const renderItensList = (list, index) => {
        return list.itens.map((item, index) => {
            return <li key={index} >{item}</li>
        })
    }


    return(
        <div className={`${props.classes.list}  ${props.anotherClassName}`} >
            { renderLists(props.objectList) }
        </div>
    )
}

List.propTypes = {
    objectList: PropTypes.object.isRequired,
    anotherClassName: PropTypes.string,
    classes: PropTypes.object
}

export default injectSheet(styles)(List)