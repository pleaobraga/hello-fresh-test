import React from 'react'
import { shallow, mount } from 'enzyme'
import { List } from '../List'

let props = {
    objectList: { 
        0: {
            title: "teste",
            itens: [ 'rice', 'egg', 'beans' ],
        }
    },
    anotherClassName: "",
    classes: {list: ""}
}

describe('List', () => {

    const list = shallow(<List {...props} />)
    const listMount = mount(<List {...props} />)

    it('renders properly', () => {
        expect(list).toMatchSnapshot()
    })

    describe('when list have one object', () => {

        it('should Render one wrapper list', () => {
            expect(listMount.find('.list-item').length).toBe(1)
        })

        it('should Render title', () => {
            expect(listMount.find('h3').exists()).toBe(true)
        })

        it('should Render one list', () => {
            expect(listMount.find('ul').length).toBe(1)
        })

        it('should Render 3 itens', () => {
            expect(listMount.find('li').length).toBe(3)
        })

        describe('when dont have title', () => {
            
            props.objectList[0].title = ""
            const listMount = mount(<List {...props} />)

            it('should Render title', () => {
                expect(listMount.find('h3').exists()).toBe(false)
            })
            
        })

    })

    describe('when list have 2 objects', () => {

        props.objectList[1] = {
            title: "teste",
            itens: [ 'rice', 'egg', 'beans' ],
        }

        const listMount = mount(<List {...props} />)

        it('should Render one wrapper list', () => {
            expect(listMount.find('.list-item').length).toBe(2)
        })
    })
    
})