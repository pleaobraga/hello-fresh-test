export const styles = {
    table: {
        display: 'flex',
        flexDirection: 'column',

        '& h1': {
            fontSize: '27px',
        }, 

        '& .table-body': {

            marginTop: '10px',

            '& .line-table':{
                display: 'flex',
                justifyContent: 'space-between',

                '& :first-child': {
                    textTransform: 'capitalize',
                    fontWeight: 'bold',
                    marginBottom: '5px',
                }
            }

        }, 
    }
}