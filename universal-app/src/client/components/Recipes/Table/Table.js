import React from 'react'
import { renderIfExist } from '../../../utils/utilFunctions'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/TableStyles'


/**
 * React stateless Component
 * return Table base
 * 
 * @param {object} props 
 * @returns 
 */
export const Table = (props) => {

    /**
     * render table lines
     * 
     * @param {array} tableLines 
     * @returns {jsx}
     */
    const renderTableLines = (tableLines) => {
        return tableLines.map((line, index) => {
            return(
                <div key={index} className="line-table" >
                    <span key={Object.keys(line)[0]} >{Object.keys(line)[0]}</span>
                    <span key={Object.values(line)[0]}>{Object.values(line)[0]}</span>
                </div>
            )
        })
    }

    return (
        <div className={`${props.classes.table} ${props.anotherClassName}`} >
            { renderIfExist(props.title, "h1") }
            <div className="table-body" >
                { renderTableLines(props.tableLines) }
            </div>
        </div>
    )
}

Table.propTypes ={
    title: PropTypes.string,
    tableLines: PropTypes.arrayOf(PropTypes.object),
    text: PropTypes.string
}

export default injectSheet(styles)(Table)