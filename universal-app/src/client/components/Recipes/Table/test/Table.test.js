import React from 'react'
import { shallow, mount } from 'enzyme'
import { Table } from '../Table'

let props = {
    tableLines: [
       { test: 'test'},
       { test1: 'test1'}
    ],
    title: "test Title",
    
    classes: {recieDetail: {} }
}

describe('Table', () => {

    const table = shallow(<Table {...props} />)

    it('renders properly', () => {
        expect(table).toMatchSnapshot()
    })

    describe('when send all properties', () => {
        
        it('should render title', () => {
            expect(table.find('h1').exists()).toBe(true)
        })

        it('should render itens table', () => {
            expect(table.find('.line-table').exists()).toBe(true)
        })
        
        it('should render 2 lines per item', () => {
            expect(table.find('span').length).toBe(4)
        })
    })


    describe('when dont send title', () => {

        props.title = ''
        const table = shallow(<Table {...props} />)
        
        it('should render title', () => {
            expect(table.find('h1').exists()).toBe(false)
        })

        it('should render itens table', () => {
            expect(table.find('.line-table').exists()).toBe(true)
        })
    })
})
