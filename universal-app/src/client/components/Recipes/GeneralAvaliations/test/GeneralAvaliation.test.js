import React from 'react'
import { shallow, mount } from 'enzyme'
import { GeneralAvaliations } from '../GeneralAvaliations'

let props = {
    favorites: 10,
    ratings: 3,
    id: "1",
    rating: 2,
    classes: { generalAv: {} }
}

describe('GeneralAvaliations', () => {

    const generalAvaliations = shallow(<GeneralAvaliations {...props} />)

    it('renders properly', () => {
        expect(generalAvaliations).toMatchSnapshot()
    })

    it('should render FullHeart', () => {
        expect(generalAvaliations.find('FaHeart').exists()).toBe(true)
    })

    it('should show the correct favorites text', () => {
        expect(generalAvaliations.find('.favorite-area span').text()).toBe(`${props.favorites} favorites`)

    })

    it('should render Rating', () => {
        expect(generalAvaliations.find('.Rating').exists()).toBe(true)
    })

    it('should show the correct favorites text', () => {
        expect(generalAvaliations.find('.score-area span').text()).toBe(`${props.ratings} votes`)

    })

})