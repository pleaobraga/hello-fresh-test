import React from 'react'
import Rating from '../Rating/Rating'
import FullHeart from 'react-icons/lib/fa/heart'
import { isEmpty, renderNumber, pluralWord } from '../../../utils/utilFunctions'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/GeneralAvaliationsStyles'

/**
 * React stateless component
 * return visual elements about general favorites and
 * general rating informations
 * 
 * @param {any} props 
 * @returns {jsx}
 */
export const GeneralAvaliations = (props) => {

    let {
        favorites,
        ratings,
        id,
        rating,
        classes,
    } = props

    
    return(
        <div className={classes.generalAv} >
            <div className="favorite-area" >
                <FullHeart />
                {/* render the recipes favorites number and applies plural in the right cases */}
                <span>{`${renderNumber(favorites)} ${pluralWord(favorites,"favorite")}`}</span>
            </div>
            <div className="score-area" >
                <Rating 
                    className="Rating"
                    idRating={`${id}-average`}
                    idRecipe={id}
                    rating={rating}
                    isVoting={false}
                />
                {/* render the recipes ratings number and applies plural in the right cases */}
                <span>{`${renderNumber(ratings)} ${pluralWord(ratings,"vote")}`}</span>
            </div>
        </div>
    )
}

GeneralAvaliations.propTypes = {
    favorites: PropTypes.number.isRequired,
    ratings: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    rating: PropTypes.number.isRequired,
    classes: PropTypes.object
}


export default injectSheet(styles)(GeneralAvaliations)