const container = {
    display: 'flex',
}

export const styles = {
    generalAv: {
        extend: container,
        marginTop: '10px',
        justifyContent: 'space-around',
        width: '100%',

        '& .favorite-area': {
            extend: container,

            '& svg': {
                color: '#000!important',
            }
        },

        '& .score-area': {
            extend: container,

            '& svg': {
                width: '20px',
                height: '20px',
                color: '#000!important'
            }
        },

        '& span': {
            marginLeft: '5px'
        }
    }
}