import React from 'react'
import Table from '../Table/Table'
import { addPropertyToArray } from '../../../utils/utilFunctions'
import _ from 'lodash'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import { styles } from './styles/NutritionTableStyles'

/**
 * React stateless component
 * return nutritional information 
 * table about this recipe
 * 
 * @param {object} props 
 * @returns {jsx}
 */
export const NutritionTable = (props) => {

    let tableLines = [],
        title = "Nutrition Value" 

    /**
     * populate the tablelines
     * array with nutritional information
     * and its names 
     * 
     * @param {object} recipe 
     */
    const createTableLinesProperty = (recipe) => {

        let {
            calories,
            fats,
            carbos,
            fibers,
            proteins
        } = recipe,
            nutritionArray = [
                { property: calories, name: "calories" },
                { property: fats, name: "fats" },
                { property: carbos, name: "carbos" },
                { property: fibers, name: "fibers" },
                { property: proteins, name: "proteins" }
            ]

        //add to tablelines the not null properties
        nutritionArray.forEach(item => {
            addPropertyToArray(item.property, item.name, tableLines)
        })
    }

    createTableLinesProperty(props.recipe)



    if(!_.isEmpty(tableLines)) {
        return(
            <Table 
                tableLines={tableLines}
                title={title}
                anotherClassName={props.classes.table}
            />
        )
    }

    return null
}

NutritionTable.propTypes = {
    recipe: PropTypes.object.isRequired
}

export default injectSheet(styles)(NutritionTable)

