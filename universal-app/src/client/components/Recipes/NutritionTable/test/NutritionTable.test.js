import React from 'react'
import { shallow, mount } from 'enzyme'
import { NutritionTable } from '../NutritionTable'

let props = {
    recipe: {
        calories: '234 kcal',
        fats: '8 g',
        carbos: '18 g',
        fibers:'7 g',
        proteins: '20 g'
    },
    classes: {list: ""}
}

describe('NutritionTable', () => {

    const nutritionTable = shallow(<NutritionTable {...props} />)
    const nutritionTableMount = mount(<NutritionTable {...props} />)

    it('renders properly', () => {
        expect(nutritionTable).toMatchSnapshot()
    })

    it('should tablelines has length = 5', () => {
        expect(nutritionTableMount.find('Table').props().tableLines.length).toBe(5);
    })

    describe('when has 3 tablelines', () => {

        props.recipe = {
            calories: '234 kcal',
            fats: '8 g',
            carbos: '18 g'
        }
        const nutritionTableMount = mount(<NutritionTable {...props} />)

        it('should have only 3 tablelines', () => {
            expect(nutritionTableMount.find('Table').props().tableLines.length).toBe(3)
        })
    })

    describe('when there is no tableLines', () => {

        props.recipe = {}
        const nutritionTableMount = mount(<NutritionTable {...props} />)


        it('should render nothing', () => {
            expect(nutritionTableMount.find('Table').exists()).toBe(false)
        })
    })
})