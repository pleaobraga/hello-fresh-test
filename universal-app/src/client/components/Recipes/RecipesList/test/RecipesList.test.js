import React from 'react'
import { shallow, mount } from 'enzyme'
import { RecipesList } from '../RecipesList'

const voteRecipe = jest.fn()

let props = {
    recipes:[
        {Teste: "teste"},
        {Teste: "teste1"},
    ],
    cardView: true,
    
    classes: {recieDetail: {} }
}

describe('RecipesList', () => {

    const recipesList = shallow(<RecipesList {...props} />)


    it('renders properly', () => {
        expect(recipesList).toMatchSnapshot()
    })

    describe('when card view is true', () => {

        it('should render Recipe Card', () => {
            expect(recipesList.find('.recipe-card').exists()).toBe(true)
        })

        it('should not render Recipe Detail', () => {
            expect(recipesList.find('.recipe-detail').exists()).toBe(false)
        })
    })

    describe('when card view is false', () => {

        props.cardView = false
        const recipesList = shallow(<RecipesList {...props} />)


        it('should not render Recipe Card', () => {
            expect(recipesList.find('.recipe-card').exists()).toBe(false)
        })

        it('should render Recipe Detail', () => {
            expect(recipesList.find('.recipe-detail').exists()).toBe(true)
        })
    })
})