export const styles = {
    recipeList: {
        display: 'flex',
        width: '100%',
        maxWidth: '1100px',
        flexWrap: 'wrap',
        justifyContent: 'center',
    }
}