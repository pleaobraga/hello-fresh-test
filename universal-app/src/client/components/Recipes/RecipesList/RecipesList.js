import React from 'react'
import RecipeCard from '../RecipeCard/RecipeCard'
import RecipeDetail from '../RecipeDetail/RecipeDetail'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import { styles } from './styles/RecipeListStyles'


/**
 * React stateless component
 * render a Recipes list
 * 
 * @param {object} props 
 * @returns {jsx}
 */
export const RecipesList = (props) => {

    /**
     * render Recipe cards List
     * 
     * @param {object} recipes 
     * @returns {jsx}
     */
    const renderRecipeCardsList = (recipes) => {
        return recipes.map((recipe, index) => {
            return (
                <RecipeCard 
                    key={index} 
                    recipe={recipe} 
                    className="recipe-card"
                />
            )
        })
    }

   /**
    * render Recipe Details List
    * 
    * @param {object} recipes 
    * @returns {jsx}
    */
   const renderRecipeDetailList = (recipes) => {
        return recipes.map((recipe, index) => {
            return (
                <RecipeDetail 
                    key={index} 
                    recipe={recipe} 
                    className="recipe-detail"
                />
            )
        })
    }

    return (
        <div className={props.classes.recipeList} >
            {
                props.cardView 
                ? renderRecipeCardsList(props.recipes)
                : renderRecipeDetailList(props.recipes)
            }    
        </div>
    )
}

RecipesList.propTypes = {
    recipes: PropTypes.array,
    cardView: PropTypes.bool.isRequired
}

export default injectSheet(styles)(RecipesList)
