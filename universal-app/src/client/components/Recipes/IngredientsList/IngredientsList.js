import React from 'react'
import List from '../List/List'
import _ from 'lodash'
import PropTypes from 'prop-types'


/**
 * React stateless component
 * return ingredients list 
 * and the ingredients not delivered 
 * 
 * @param {object} props 
 * @returns {jsx}
 */
export const IngredientsList = (props) => {

    let {
        ingredients,
        undeliverable_ingredients
    } = props.recipe,
        objectList = {}


    //check if ingredients exist
    if( !_.isEmpty(ingredients)) {
        
        //create object with ingredient list
        objectList[0] = {
            title: "Ingredient List",
            itens: [...ingredients]
        }

        //add to the object undelivery ingredients if exists
        if(!_.isEmpty(undeliverable_ingredients)) {

            objectList[1] = {
                title: "Undeliverable Ingredients",
                itens: [...undeliverable_ingredients]
            }
        }

        return (
            <List objectList={objectList}/>
        )
    } 
        
    return null
}

IngredientsList.propTypes = {
    recipe: PropTypes.object
}

export default IngredientsList