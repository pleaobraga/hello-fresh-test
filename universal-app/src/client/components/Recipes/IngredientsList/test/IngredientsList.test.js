import React from 'react'
import { shallow, mount } from 'enzyme'
import { IngredientsList } from '../IngredientsList'

let props = {
    recipe: { 
        ingredients: [ 'rice', 'egg', 'beans' ],
        undeliverable_ingredients: [ 'rice', 'egg', 'beans' ],
    }
}

describe('IngredientsList', () => {

    const ingredientsList = shallow(<IngredientsList {...props} />)
    const ingredientsListMount = mount(<IngredientsList {...props} />)

    it('renders properly', () => {
        expect(ingredientsList).toMatchSnapshot()
    })


    it('should render list', () => {
        expect(ingredientsListMount.children().length).toBe(1)
    })

    describe('when dont have a ingredients itens', () =>{
        
        props.recipe.ingredients = []
        const ingredientsListMount = mount(<IngredientsList {...props} />)

        it('should not render ', () => {
            expect(ingredientsListMount.children().length).toBe(0)
        })
    })
})