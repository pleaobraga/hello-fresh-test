import * as utilFunc from '../utilFunctions'
import React from 'react'


describe('Util Functions', () => {

    describe('test getStringNumbers', () => {

        const testCase = [
            {string: 'jh23', ExpectedValue: "23" },
            {string: 'owefn73', ExpectedValue: "73" },
            {string: '5jh23', ExpectedValue: "523" },
            {string: '4jh3', ExpectedValue: "43" },
            {string: 'j23h', ExpectedValue: "23" },
            {string: '1j23h', ExpectedValue: "123" },
            {string: '11jh', ExpectedValue: "11" },
        ]

        testCase.map(({string, ExpectedValue}) => {
            it(`string ${string} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.getStringNumbers(string)).toBe(ExpectedValue)
            })
        })
    })

    describe('test formatDuration', () => {
        const testCase = [
            {string: '34M', ExpectedValue: "34 minutes" },
            {string: '40M', ExpectedValue: "40 minutes" },
            {string: '1M', ExpectedValue: "1 minute" },
            {string: '0M', ExpectedValue: "0 minutes" },
        ]

        testCase.map(({string, ExpectedValue}) => {
            it(`string ${string} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.formatDuration(string)).toBe(ExpectedValue)
            })
        })
    })

    describe('test isEmpty', () => {

        const testCase = [
            {element: '', ExpectedValue: true },
            {element: null, ExpectedValue: true },
            {element: undefined, ExpectedValue: true },
            {element: 'jh23', ExpectedValue: false },
            {element: 1, ExpectedValue: false },
            
        ]

        testCase.map(({element, ExpectedValue}) => {
            it(`element ${element} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.isEmpty(element)).toBe(ExpectedValue)
            })
        })
    })

    describe('test renderIfExist', () => {

        const testCase = [
            {element: '', ExpectedValue: true },
            {element: null, ExpectedValue: true },
            {element: undefined, ExpectedValue: true },
            {element: 'jh23', ExpectedValue: false },
            {element: 1, ExpectedValue: false },
            
        ]

        testCase.map(({element, ExpectedValue}) => {
            it(`element ${element} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.isEmpty(element)).toBe(ExpectedValue)
            })
        })
    })

    describe('test renderIfExist', () => {

        const testCase = [
            {element: ['test', 'h1'], ExpectedValue: (<h1 className="">test</h1>) },
            {element: ['te', 'h2'], ExpectedValue: (<h2 className="">te</h2>) },
            {element: ['test cdsc dcsd', 'span','test' ], ExpectedValue: (<span className="test">test cdsc dcsd</span>) },
        ]

        testCase.map(({element, ExpectedValue}) => {
            it(`element property ${element[0]}`, () => {
                expect(utilFunc.renderIfExist(...element)).toEqual(ExpectedValue)
            })

        })
    })

    describe('test addPropertyToArray', () => {

        const testCase = [
            {element: ['teste', 'teste'], ExpectedValue: [{teste: 'teste'}]  },
            {element: [true, 'ok'], ExpectedValue: [{ok: true}]  },
            {element: [3, 'numb'], ExpectedValue: [{numb: 3}]  },
        ]

        
        testCase.map(({element, ExpectedValue}) => {
            it(`element ${element} expected: ${ExpectedValue}`, () => {
                const array = []
                utilFunc.addPropertyToArray(...element, array)
                expect(array).toEqual(ExpectedValue)
            })
        })
    })

    describe('test formatWeek', () => {

        const testCase = [
            { string: ["2014-W25"], ExpectedValue: ['25 week in 2014'] },
            { string: ["2018-W5"], ExpectedValue: ['5 week in 2018'] },
            { string: ["2020-W21"], ExpectedValue: ['21 week in 2020'] },
        ]

        testCase.map(({string, ExpectedValue}) => {
            it(`string ${string[0]} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.formatWeek(string)).toEqual(ExpectedValue)
            })
        })
    })

    describe('test renderNumber', () => {

        const testCase = [
            {element: '', ExpectedValue: 0 },
            {element: null, ExpectedValue: 0 },
            {element: undefined, ExpectedValue: 0 },
            {element: 1, ExpectedValue: 1 },
            {element: 1000, ExpectedValue: 1000 },
            
        ]

        testCase.map(({element, ExpectedValue}) => {
            it(`element ${element} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.renderNumber(element)).toBe(ExpectedValue)
            })
        })
    })

    describe('test pluralWord', () => {

        const testCase = [
            {element: [ 1, 'test' ], ExpectedValue: 'test' },
            {element: [ 0, 'test' ], ExpectedValue: 'tests' },
            {element: [ 11, 'test' ], ExpectedValue: 'tests' },
        ]

        testCase.map(({element, ExpectedValue}) => {
            it(`element ${element} expected: ${ExpectedValue}`, () => {
                expect(utilFunc.pluralWord(...element)).toBe(ExpectedValue)
            })
        })
    })

})