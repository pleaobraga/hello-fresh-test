import { URL_API } from './constants'
import axios from 'axios'

export const getRecipeData = () => {
    return axios.get(URL_API)
}