import React from 'react'
import _ from 'lodash'


/**
 * return numbers inside the string
 * 
 * @param {string} string 
 * @returns {int}
 */
export const getStringNumbers = (string) => {
    return isEmpty(string.match(/\d+/)) ? null : string.match(/\d+/g).join('')
}


/**
 *  return recipe Duration formated 
 * 
 * @param {any} stringDuration 
 * @returns {string}
 */
export const formatDuration = (stringDuration = "s") => {
    let duration = getStringNumbers(stringDuration),
        unity = stringDuration[stringDuration.length -1 ],
        fullUnity

    switch(unity) {

        case "M": 
            fullUnity = "minute"  
            break

        default:
            fullUnity=""
            break
    }

    return `${duration} ${duration != 1 ? `${fullUnity}s`: fullUnity}`
} 


/**
 * verify if this element
 * is empty
 * 
 * @param {any} element 
 * @returns 
 */
export const isEmpty = (element) => {
    return element === null || element === undefined || element === "" 
}

/**
 * return a custom html element
 * with its property if 
 * is not null or undefined
 * 
 * @param {any} property 
 * @param {string} htmlTag 
 * @returns 
 */
export const renderIfExist = (property, htmlTag, classAtribute = '') => {

    const CustomTag = `${htmlTag}`;

    return isEmpty(property) ? null : <CustomTag className={classAtribute}>{property}</CustomTag>
}


/**
 * create a personalized object
 * and add it to array
 * 
 * @param {any} property 
 * @param {string} name 
 * @param {array} array 
 */
export const addPropertyToArray = ( property, name, array ) => {
    if(!isEmpty(property)) {
        let obj = {}
        
        obj[name] = property
        array.push(obj)
    }     
}


/**
 * format string week
 * 
 * @param {string} weeks 
 * @returns {string}
 */
export const formatWeek = (weeks) => {
    if(!_.isEmpty(weeks)) {
        return weeks.map(week => {
            
            week = week.split("-W")

            return `${week[1]} week in ${week[0]}`
        })
    }
} 


/**
 * return number even 
 * variable is empty
 * 
 * @param {int} number 
 * @returns {int}
 */
export const renderNumber = (number) => {
    return isEmpty(number) ? 0 : number
}

/**
 * render correctly plural word
 * in context
 * add plural if necessary 
 * 
 * @param {number} number 
 * @param {string} word 
 * @param {string} pluralTermination 
 * @returns {string}
 */
export const pluralWord = (number, word, pluralTermination = 's' ) => {
    return `${word}${renderNumber(number) === 1 ? '' : pluralTermination }`
} 