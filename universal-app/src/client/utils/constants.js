export const FETCH_RECIPES    = "FETCH_RECIPES"
export const VOTE_RECIPE      = "VOTE_RECIPE"
export const TOGGLE_FAVORITE  = "TOGGLE_FAVORITE"
export const URL_API          = "http://localhost:3333"
