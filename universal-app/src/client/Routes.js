import React from 'react'
import Login from './pages/Login/Login'
import Recipes from './pages/Recipes/Recipes'
import NotFoundPage from './pages/NotFound/NotFound'

export default [
    {
        ...Login,
        path: '/',
        exact: true
    },
    {
        ...Recipes,
        path: '/recipes',
        exact: true
    },
    {
        ...NotFoundPage
    }
        
]

