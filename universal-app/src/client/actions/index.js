import { FETCH_RECIPES, TOGGLE_FAVORITE, VOTE_RECIPE } from '../utils/constants'
import recipes from '../utils/recipes.json'
import { getRecipeData } from '../utils/RecipesApi'


/**
 * dispatch recipes data 
 * 
 */
/*export const fetchRecipes = () => (dispatch, getState)  => {
    const res = {
        data: recipes
    }

    dispatch({
        type: FETCH_RECIPES,
        payload: res
    })
}*/

/**
 * dispatch recipes data 
 * 
 */
export const fetchRecipes = () => async (dispatch, getState)  => {
    
    const res = await getRecipeData()
    
    dispatch({
        type: FETCH_RECIPES,
        payload: res
    })
}


/**
 * Dispatch the highlight value
 * 
 * @param {string} id 
 * @param {bool} value 
 */
export const highlight = (id, value) => (dispatch, getState) => {
    dispatch({
        type: TOGGLE_FAVORITE,
        id,
        value
    })
}

/**
 * dispatch the vote 
 * 
 * @param {string} id 
 * @param {integer} vote 
 */
export const voteRecipe = (id, vote) => (dispatch, getState) => {
    dispatch({
        type: VOTE_RECIPE,
        id,
        vote
    })
}



