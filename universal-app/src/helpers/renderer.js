import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { renderRoutes } from 'react-router-config'
import Routes from '../client/Routes'
import serialize from 'serialize-javascript'
import {JssProvider, SheetsRegistry} from 'react-jss'
import { cssReset } from './cssReset'


export default ({path}, store = {}, context) => {

    const sheets = new SheetsRegistry()

    const content = renderToString(
        <Provider store={store} >
            <JssProvider registry={sheets}>
                <StaticRouter 
                    location={path} 
                    context={context} 
                >
                    <div>{renderRoutes(Routes)}</div>
                </StaticRouter>
            </JssProvider>
        </Provider>   
    )

    return `
        <html>
            <head>
                <style>${cssReset}</style>
                <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
                <meta name="viewport" content="width=device-width">
            </head>
            <body>
                <div id="root" >${content}</div>
                <script>
                    window.INITIAL_STATE = ${serialize(store.getState())}
                </script>
            </body>
            <script src="bundle.js" ></script>
        </html>
    `
}