# Universal app

This App is an Isomporphic/Universal React App 

* This App has two pages
    - Login
    - Recipes

* Recipes page you can see recipes details in two different ways:
cards or list


## Start Application

To get started Application right away:

* Check your node and npm versions.
    - node : 8.10.0
    - npm : 5.7.1

* Install and start the APP
    - `npm install`
    - `npm run dev`

The app will open a new web page automatically
initializing the app

* Run Tests:
    - `npm test`

The Api uses port the 3000 as default

**Do not forget to run the Recipe-Api before starts.**
The recipes data comes from the api

## Public Link

To use the application, click on 
this link https://frontend-test-pedro-braga.herokuapp.com


this application is not using the external link to get the data to do 
not expose the `recipes.json` code to anyone

### Explanations

- The project was based on **SOLIDS** principles
- The project is an Isomporphic/Universal React App 
- Uses wabpack to generate the server and front code
- The main goal was finish the basics to start the bonus itens, except Isomporphic/Universal item
- uses a Hello Fresh Page as desing base like colors, some components.

**The projects folder**

**Productions folders**
- build: code generate by webpack
- public: code generate by webpack

**Dev folders**
- src : contains entire code
- helpers: files which help the backend server like render the application
- index: is a entry point from server side

 **src folders**
- client: contain entire front end aplications 
- client Components: contains all application components  
- client Pages: is a folder where renders the components and organize it in the page
- client Utils: is a folder that has a lot of shared functions to help the components

**Components folders**
Base Components
- Form/InputForm
- Form/FormBase
- Reciped/List
- Reciped/Table


The aplication starts with the server create Redux store and resolve all possible promisses 
after get the file send the response to browser who starts the front end aplication hydrating the code generate by backend.

This solution dont break in case some promisse did not work , so if you start the aplication without Recipe-Api it will not crash, just will not render the recipes.

If you try acess some wrong wrote an message will display

**Login Page /**

This page was based on hello fresh page and you just need enter a valid email with a
non-empty password, the button will be clickable and you can go through the application


**Recipes Page /recipes**

- This page shows the recipes.json entire information,
- Has 2 ways to show the code, Card View and List view 
- the itens is displayed to show the recipe.json in a frendly user way  
- you only can interact on List mode

Every single page is responsible. 
The front-end code is almost tested
The Code is Commented

 **Main Third Libraries**
- Lodash
- CSS-in-Js
- jest
- axios
- nodemon
- react
- redux
- webpack


### Future Improvements

- Create more tests
- Use some authentication after login
- Create a search bar on the Recipes page to find them faster
- Create filters to find recipes faster
- Create a single recipe view page
- Show user coordinates with google Api


